package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import view.PanelSearch;
import model.Book;
import model.BookstoreModel;

public class SearchController {

	private BookstoreModel model;
	private PanelSearch view;
	
	public SearchController(BookstoreModel model, PanelSearch view) {
		this.model = model;
		this.view = view;
		
		view.addbyAuthorListener(new byAuthorListener());
		view.addbyTitleListener(new byTitleListener());
		view.addbyTypeListener(new byTypeListener());
		view.addsellListener(new sellListener());
	}
	
	class byAuthorListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String userInput = "";
            try {
                userInput = view.getAuthor();
                ArrayList <Book> al = model.getBookList();
                
                for (Book i : al)
                
                	if (userInput.equals(i.getAuthor()))
                	{
                		view.setId(Integer.toString(i.getId()));
                		view.setAuthor(i.getAuthor());
                		view.setTitle(i.getTitle());
                		view.setType(i.getGenre());
                		view.setQuant(Integer.toString(i.getQuantity()));
                		view.setPrice(Integer.toString(i.getPrice()));
                		break;
                	}
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class byTitleListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String userInput = "";
            try {
                userInput = view.getTitle();
                ArrayList <Book> al = model.getBookList();
                
                for (Book i : al)
                
                	if (userInput.equals(i.getTitle()))
                	{
                		view.setId(Integer.toString(i.getId()));
                		view.setAuthor(i.getAuthor());
                		view.setTitle(i.getTitle());
                		view.setType(i.getGenre());
                		view.setQuant(Integer.toString(i.getQuantity()));
                		view.setPrice(Integer.toString(i.getPrice()));
                		break;
                	}
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class byTypeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String userInput = "";
            try {
                userInput = view.getType();
                ArrayList <Book> al = model.getBookList();
                
                for (Book i : al)
                
                	if (userInput.equals(i.getGenre()))
                	{
                		view.setId(Integer.toString(i.getId()));
                		view.setAuthor(i.getAuthor());
                		view.setType(i.getGenre());
                		view.setTitle(i.getTitle());
                		view.setQuant(Integer.toString(i.getQuantity()));
                		view.setPrice(Integer.toString(i.getPrice()));
                		break;
                	}
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class sellListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String userInput = "";
            try {
                userInput = view.getTitle();
                ArrayList <Book> al = model.getBookList();
                
                for (Book i : al)
                
                	if (userInput.equals(i.getTitle()))
                	{
                		model.sell(i.getId());
                		view.setId(Integer.toString(i.getId()));
                		view.setAuthor(i.getAuthor());
                		view.setType(i.getGenre());
                		view.setTitle(i.getTitle());
                		view.setQuant(Integer.toString(i.getQuantity()));
                		view.setPrice(Integer.toString(i.getPrice()));
                		break;
                	}
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
}
