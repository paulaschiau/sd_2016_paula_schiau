package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import view.PanelEmployee;
import model.BookstoreModel;
import model.Employee;

public class EmployeeController {

	private BookstoreModel model;
	private PanelEmployee view;
	
	public EmployeeController(BookstoreModel model, PanelEmployee view) {
		this.model = model;
		this.view = view;
		
		view.addinsertListener(new insertListener());
		view.addsearchListener(new findListener());
		view.adddeleteListener(new deleteListener());
		view.addupdateListener(new updateListener());		
	}
	
	class insertListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            
            
            
            
            try {
            	
            	int id=Integer.parseInt(view.getId());
                String nume=view.getName();
                String adresa=view.getAddress();
                String pass=view.getPass();
               
            	
            	Employee x = new Employee(id,nume,adresa,pass);
            	model.insertEmployee(x);
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class findListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
         
            try {
            	
            	int id=Integer.parseInt(view.getId());
                ArrayList <Employee> al = model.getEmployeeList();
                
                for (Employee i : al)
                
                	if (id==i.getId())
                	{
                		view.setId(Integer.toString(i.getId()));
                		view.setName(i.getName());
                		view.setAddress(i.getAddress());
                		view.setPass(i.getPass());
                		
                		break;
                	}
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class deleteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
          
            try {
            	
            	int id=Integer.parseInt(view.getId());
            	model.deleteEmployee(id);
                
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class updateListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           
            try {
            	
            	int id=Integer.parseInt(view.getId());
                String nume=view.getName();
                String adresa=view.getAddress();
                String pass=view.getPass();
               
            	
            	Employee x = new Employee(id,nume,adresa,pass);
            	
            	model.updateEmployee(x);
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	
	
}
