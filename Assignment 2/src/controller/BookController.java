package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import view.PanelBooks;
import model.Book;
import model.BookstoreModel;
import model.Report;
import model.ReportFactory;

public class BookController {

	private BookstoreModel model;
	private PanelBooks view;
	
	public BookController(BookstoreModel model, PanelBooks view) {
		this.model = model;
		this.view = view;
		
		view.addinsertListener(new insertListener());
		view.addsearchListener(new findListener());
		view.adddeleteListener(new deleteListener());
		view.addupdateListener(new updateListener());
		view.addreportListener(new reportListener());
	}
	
	class insertListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            
            
            
            
            try {
            	
            	int id=Integer.parseInt(view.getId());
                String title=view.getTitle();
                String author=view.getAuthor();
                String genre=view.getType();
                int cant=Integer.parseInt(view.getQuant());
                int pret=Integer.parseInt(view.getPrice());
            	
            	Book x = new Book(id,title,author,genre,cant,pret);
            	model.insertBook(x);
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class findListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
         
            try {
            	
            	int id=Integer.parseInt(view.getId());
                ArrayList <Book> al = model.getBookList();
                
                for (Book i : al)
                
                	if (id==i.getId())
                	{
                		view.setId(Integer.toString(i.getId()));
                		view.setAuthor(i.getAuthor());
                		view.setTitle(i.getTitle());
                		view.setType(i.getGenre());
                		view.setQuant(Integer.toString(i.getQuantity()));
                		view.setPrice(Integer.toString(i.getPrice()));
                		break;
                	}
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class deleteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
          
            try {
            	
            	int id=Integer.parseInt(view.getId());
            	model.deleteBook(id);
                
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class updateListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           
            try {
            	int id=Integer.parseInt(view.getId());
                String titlu=view.getTitle();
                String autor=view.getAuthor();
                String gen=view.getType();
                int cant=Integer.parseInt(view.getQuant());
                int pret=Integer.parseInt(view.getPrice());
            	
            	Book x = new Book(id,titlu,autor,gen,cant,pret);
            	
            	model.updateBook(x);
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
	class reportListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           
            try {
            	
            	Report x = new ReportFactory().createRaport();
            	x.creareRaport(model.getBookList());
                
                
                
                
            } catch (Exception ex) {
				ex.printStackTrace();
			}
        }
    }
	
}
