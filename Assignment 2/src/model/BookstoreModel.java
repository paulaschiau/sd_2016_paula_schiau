package model;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;


import java.io.File;
import java.util.ArrayList;
import java.util.Observable;


public class BookstoreModel extends Observable {

	private ArrayList<Employee> employeeList;
	private ArrayList<Book> bookList;

	public ArrayList<Employee> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(ArrayList<Employee> emplList) {
		this.employeeList = emplList;
		setChanged();
		notifyObservers();
	}

	public ArrayList<Book> getBookList() {
		return bookList;
	}

	public void setBookList(ArrayList<Book> bookList) {
		this.bookList = bookList;
		setChanged();
		notifyObservers();
	}

	public ArrayList<Employee> readXMLfileEmployees() {
		try {
			ArrayList<Employee> lista = new ArrayList<Employee>();

			File xmlFile = new File("employees.xml");
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document doc = documentBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
		
			NodeList nodeList = doc.getElementsByTagName("employee");
			
			for (int itr = 0; itr < nodeList.getLength(); itr++) {

				Node node = nodeList.item(itr);
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) node;
	
					Employee e = new Employee(Integer.parseInt(eElement
							.getAttribute("id")), eElement
							.getElementsByTagName("name").item(0)
							.getTextContent(), eElement
							.getElementsByTagName("address").item(0)
							.getTextContent(), eElement
							.getElementsByTagName("pass").item(0)
							.getTextContent());
					lista.add(e);
				}
			}
			employeeList = lista;
			setChanged();
			notifyObservers();
			return lista;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void insertEmployee(Employee x) {
		employeeList.add(x);
		writeXmlFileEmployees(employeeList);
		setChanged();
		notifyObservers();

	}

	public Employee findEmployee(int id) {
		
		int index=0;
		
		for (Employee i : employeeList)
		{
			if (id==i.getId())
				break;
			index++;
		}
		
		return employeeList.get(index);
	}

	public void deleteEmployee(int id) {
		int index=0;
		
		for (Employee i : employeeList)
		{
			if (id==i.getId())
				break;
			index++;
		}
		employeeList.remove(index);
		writeXmlFileEmployees(employeeList);
		setChanged();
		notifyObservers();
	}

	public void updateEmployee(Employee x) {
		int index=0;
		
		for (Employee i : employeeList)
		{
			if (x.getId()==i.getId())
				break;
			index++;
		}
		employeeList.set(index, x);
		writeXmlFileEmployees(employeeList);
		setChanged();
		notifyObservers();
	}

	public void writeXmlFileEmployees(ArrayList<Employee> list) {

		try {

			DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = dFact.newDocumentBuilder();
			Document document = build.newDocument();

			Element root = document.createElement("angajati");
			document.appendChild(root);

			for (Employee e : list) {

				// employee element
				Element employee = document.createElement("employee");
				root.appendChild(employee);
				// set an attribute to employee element
				Attr attr = document.createAttribute("id");
				attr.setValue(Integer.toString(e.getId()));
				employee.setAttributeNode(attr);
				// first name 
				Element firstName = document.createElement("name");
				firstName.appendChild(document.createTextNode(e.getName()));
				employee.appendChild(firstName);
				// last name 
				Element adr = document.createElement("address");
				adr.appendChild(document.createTextNode(e.getAddress()));
				employee.appendChild(adr);

				Element pas = document.createElement("pass");
				pas.appendChild(document.createTextNode(e.getPass()));
				employee.appendChild(pas);

			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");

			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult("employees.xml");
			transformer.transform(domSource, streamResult);
			System.out.println("Done creating XML File");
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

	public ArrayList<Book> readXMLfileBooks() {
		try {
			ArrayList<Book> lista = new ArrayList<Book>();

			File xmlFile = new File("books.xml");
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document doc = documentBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("book");
			for (int itr = 0; itr < nodeList.getLength(); itr++) {

				Node node = nodeList.item(itr);
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) node;

					
					
					int id = Integer.parseInt(eElement.getAttribute("id"));
					String titlu = eElement.getElementsByTagName("title").item(0).getTextContent();
					String author = eElement.getElementsByTagName("author").item(0).getTextContent();
					String type = eElement.getElementsByTagName("type").item(0).getTextContent();
					int pret =Integer.parseInt(eElement.getElementsByTagName("price").item(0).getTextContent());
					int quant =Integer.parseInt(eElement.getElementsByTagName("quant").item(0).getTextContent());
					
					Book b= new Book(id,titlu,author,type,quant,pret);
					
					lista.add(b);
				}
			}
			bookList = lista;
			setChanged();
			notifyObservers();
			return lista;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void writeXmlFileBooks(ArrayList<Book> list) {

		try {

			DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = dFact.newDocumentBuilder();
			Document document = build.newDocument();

			Element root = document.createElement("bookstore");
			document.appendChild(root);

			for (Book e : list) {

				
				Element book = document.createElement("book");
				root.appendChild(book);
				
				Attr attr = document.createAttribute("id");
				attr.setValue(Integer.toString(e.getId()));
				book.setAttributeNode(attr);
				
				Element title = document.createElement("title");
				title.appendChild(document.createTextNode(e.getTitle()));
				book.appendChild(title);
				
				Element author = document.createElement("author");
				author.appendChild(document.createTextNode(e.getAuthor()));
				book.appendChild(author);

				Element type = document.createElement("type");
				type.appendChild(document.createTextNode(e.getGenre()));
				book.appendChild(type);
				
				Element price = document.createElement("price");
				price.appendChild(document.createTextNode(Integer.toString(e.getPrice())));
				book.appendChild(price);
				
				Element quant = document.createElement("quant");
				quant.appendChild(document.createTextNode(Integer.toString(e.getQuantity())));
				book.appendChild(quant);

			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult("books.xml");
			transformer.transform(domSource, streamResult);
			System.out.println("Done creating XML File");
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}
	
	public void insertBook(Book x) {
		bookList.add(x);
		writeXmlFileBooks(bookList);
		setChanged();
		notifyObservers();

	}

	public Book findBook(int id) {
	int index=0;
		
		for (Book i : bookList)
		{
			if (id==i.getId())
				break;
			index++;
		}
		return bookList.get(index);
	}

	public void deleteBook(int id) {
		int index=0;
		
		for (Book i : bookList)
		{
			if (id==i.getId())
				break;
			index++;
		}
		bookList.remove(index);
		writeXmlFileBooks(bookList);
		setChanged();
		notifyObservers();
	}

	public void updateBook(Book x) {
		int index=0;
		
		for (Book i : bookList)
		{
			if (x.getId()==i.getId())
				break;
			index++;
		}
		bookList.set(index, x);
		writeXmlFileBooks(bookList);
		setChanged();
		notifyObservers();
	}
	
	public String[] getAllEmployees()
	{
		readXMLfileEmployees();
		readXMLfileBooks();
		String[] s = new String[employeeList.size()];
		int j=0;
		for(Employee i : employeeList)
		{
			s[j]=i.getName();
			j++;
		}
		return s;
	}
	
	public Object[][] report() {

		
		int dim = bookList.size();
		int k = 0;

		Object[][] data = new Object[dim][6];

		for (Book i : bookList)
		{
			

				data[k][0] = i.getId();
				data[k][1] = i.getTitle();
				data[k][2] = i.getAuthor();				
				data[k][3] = i.getGenre();
				data[k][4] = i.getQuantity();
				data[k][5] = i.getPrice();
				
			k++;

		}
		
		return data;
	}
	
	public void sell (int id)
	{

		int index=0;
		Book x;
		for (Book i : bookList)
		{
			if (id==i.getId())
			{
				x=i;
				if(x.getQuantity()>=1){
					x.setQuantity(x.getQuantity()-1);
					bookList.set(index, x);
					writeXmlFileBooks(bookList);
					setChanged();
					notifyObservers();
				}
				break;
			}
				
			index++;
        	
		}
		
		
		
		
	}
	
	public boolean checkPass(String x, String pass)
	{
		for (Employee i : employeeList)
		{
			if(x.equals(i.getName()))
				if (pass.equals(i.getPass()))
					return true;
		}
		return false;
	}

	public Object[][] employeeReport() {
		int dim = employeeList.size();
		int k = 0;

		Object[][] data = new Object[dim][4];

		for (Employee i : employeeList)
		{
			

				data[k][0] = i.getId();
				data[k][1] = i.getName();
				data[k][2] = i.getAddress();				
				data[k][3] = i.getPass();
				
				
			k++;

		}
		
		return data;
	}
}
