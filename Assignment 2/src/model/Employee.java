package model;




public class Employee {

	private int id;
	private String name;
	private String address;
	private String pass;

	public Employee(int id, String name, String address, String pass) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.pass = pass;
	}

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	
	
}