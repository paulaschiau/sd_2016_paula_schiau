package model;

import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Report {

	public void creareRaport(ArrayList <Book> list) {
		try {

			DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build = dFact.newDocumentBuilder();
			Document document = build.newDocument();

			Element root = document.createElement("bookstore");
			document.appendChild(root);

			for (Book e : list) {

				if (e.getQuantity()==0)
				{
				Element book = document.createElement("book");
				root.appendChild(book);
				
				Attr attr = document.createAttribute("id");
				attr.setValue(Integer.toString(e.getId()));
				book.setAttributeNode(attr);
				
				Element title = document.createElement("title");
				title.appendChild(document.createTextNode(e.getTitle()));
				book.appendChild(title);
				
				Element author = document.createElement("author");
				author.appendChild(document.createTextNode(e.getAuthor()));
				book.appendChild(author);

				Element type = document.createElement("type");
				type.appendChild(document.createTextNode(e.getGenre()));
				book.appendChild(type);
				
				Element price = document.createElement("price");
				price.appendChild(document.createTextNode(Integer.toString(e.getPrice())));
				book.appendChild(price);
				
				Element quant = document.createElement("quant");
				quant.appendChild(document.createTextNode(Integer.toString(e.getQuantity())));
				book.appendChild(quant);
				}
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		

			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult("report.xml");
			transformer.transform(domSource, streamResult);
			System.out.println("Done creating XML File");
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
		}
	
	
}
