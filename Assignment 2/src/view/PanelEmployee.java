package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import model.BookstoreModel;



public class PanelEmployee extends JLayeredPane implements Observer{
	
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTable table ;
	private JPanel panel ;
	private JButton btnNewButton_1 = new JButton("Create");
	private JButton btnNewButton_2 = new JButton("Find");
	private JButton btnNewButton_3 = new JButton("Delete");
	private JButton btnNewButton_4 = new JButton("Update");


	private BookstoreModel model;
	

	public PanelEmployee(BookstoreModel mod) {
		
		model=mod;
		model.addObserver(this);
		
		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		

		JLabel lblNewLabel = new JLabel("ID");
		lblNewLabel.setBounds(90, 65, 66, 14);
		this.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nume");
		lblNewLabel_1.setBounds(90, 120, 66, 14);
		this.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Adresa");
		lblNewLabel_2.setBounds(90, 185, 66, 14);
		this.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Pass");
		lblNewLabel_3.setBounds(90, 250, 66, 14);
		this.add(lblNewLabel_3);
		
		
		
		textField = new JTextField();
		textField.setBounds(194, 62, 106, 20);
		this.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(194, 117, 106, 20);
		this.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(194, 183, 106, 20);
		this.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(194, 247, 106, 20);
		this.add(textField_3);
		textField_3.setColumns(10);
				
		
		panel = new JPanel();
		panel.setBounds(344, 60, 425, 215);
		panel.setBackground(Color.WHITE);
		panel.setPreferredSize(new Dimension(500, 500));
		
		String[] columnNames = {"ID","Nume", "Adresa", "Pass"};
		
		Object[][] data= model.employeeReport();
		panel.setLayout(null);
		
		table = new JTable(data, columnNames);
		panel.setLayout(null);
		panel.setLayout(new BorderLayout());
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		
		this.add(panel);
		
		
		btnNewButton_1.setBounds(90, 338, 89, 23);
		this.add(btnNewButton_1);
				
		btnNewButton_2.setBounds(256, 338, 89, 23);
		this.add(btnNewButton_2);
				
		btnNewButton_3.setBounds(430, 338, 89, 23);
		this.add(btnNewButton_3);
				
		btnNewButton_4.setBounds(621, 338, 89, 23);
		this.add(btnNewButton_4);		
		
		

	}

	public void addinsertListener(ActionListener mal) {
		btnNewButton_1.addActionListener(mal);
    }
    
	public void addsearchListener(ActionListener cal) {
    	btnNewButton_2.addActionListener(cal);
    }
	public void adddeleteListener(ActionListener cal) {
    	btnNewButton_3.addActionListener(cal);
    }
	public void addupdateListener(ActionListener cal) {
    	btnNewButton_4.addActionListener(cal);
    }
	
	
	public String getId() {
        return textField.getText();
    }
	
	
	public String getName() {
        return textField_1.getText();
    }
	
	public String getAddress() {
        return textField_2.getText();
    }
	
	public String getPass() {
        return textField_3.getText();
    }
	
	
	
	public void setId(String id) {
		textField.setText(id);
    }
	
	public void setName(String name) {
		textField_1.setText(name);
    }
	
	public void setAddress(String address) {
		textField_2.setText(address);
    }
	
	public void setPass(String pass) {
		textField_3.setText(pass);
    }
	
	
	
	@Override
	public void update(Observable o, Object arg) {
		
		String[] columnNames = {"ID","Name", "Address", "Pass"};
		
		Object[][] data= model.employeeReport();
		panel.setLayout(null);
		
		table = new JTable(data, columnNames);
		panel.setLayout(null);
		panel.setLayout(new BorderLayout());
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		
		repaint();
		
	}

}
