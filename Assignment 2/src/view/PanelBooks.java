package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import model.BookstoreModel;



public class PanelBooks extends JLayeredPane implements Observer{
	
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTable table;
	private JPanel panel;
	private JButton btnNewButton_1 = new JButton("Create");
	private JButton btnNewButton_2 = new JButton("Find by ID");
	private JButton btnNewButton_3 = new JButton("Delete");
	private JButton btnNewButton_4 = new JButton("Update");
	private JButton btnNewButton_5 = new JButton("Report");

	private BookstoreModel model;
	

	public PanelBooks(BookstoreModel mod) {
		
		model=mod;
		model.addObserver(this);
		
		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		

		JLabel lblNewLabel = new JLabel("Book id");
		lblNewLabel.setBounds(90, 65, 66, 14);
		this.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Title");
		lblNewLabel_1.setBounds(90, 108, 66, 14);
		this.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Author");
		lblNewLabel_2.setBounds(90, 151, 66, 14);
		this.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Type");
		lblNewLabel_3.setBounds(90, 194, 66, 14);
		this.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Quantity");
		lblNewLabel_4.setBounds(90, 237, 66, 14);
		this.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Price");
		lblNewLabel_5.setBounds(90, 280, 66, 17);
		this.add(lblNewLabel_5);
		
		textField = new JTextField();
		textField.setBounds(194, 62, 106, 20);
		this.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(194, 105, 106, 20);
		this.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(194, 148, 106, 20);
		this.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(194, 191, 106, 20);
		this.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(194, 234, 106, 20);
		this.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(192, 277, 108, 20);
		this.add(textField_5);
		textField_5.setColumns(10);
		
	
		panel = new JPanel();
		panel.setBounds(344, 69, 425, 215);
		panel.setBackground(Color.WHITE);
		panel.setPreferredSize(new Dimension(500, 500));
		
		String[] columnNames = {"Book id","Title", "Author", "Type","Quantity","Price"};
		
		Object[][] data= model.report();
		panel.setLayout(null);
		
		table = new JTable(data, columnNames);
		panel.setLayout(null);
		panel.setLayout(new BorderLayout());
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		
		this.add(panel);
		
		
		btnNewButton_1.setBounds(90, 338, 89, 23);
		this.add(btnNewButton_1);
				
		btnNewButton_2.setBounds(220, 338, 89, 23);
		this.add(btnNewButton_2);
				
		btnNewButton_3.setBounds(350, 338, 89, 23);
		this.add(btnNewButton_3);
				
		btnNewButton_4.setBounds(480, 338, 89, 23);
		this.add(btnNewButton_4);		
		
		btnNewButton_5.setBounds(610, 338, 89, 23);
		this.add(btnNewButton_5);
		

	}

	public void addinsertListener(ActionListener mal) {
		btnNewButton_1.addActionListener(mal);
    }
    
	public void addsearchListener(ActionListener cal) {
    	btnNewButton_2.addActionListener(cal);
    }
	public void adddeleteListener(ActionListener cal) {
    	btnNewButton_3.addActionListener(cal);
    }
	public void addupdateListener(ActionListener cal) {
    	btnNewButton_4.addActionListener(cal);
    }
	public void addreportListener(ActionListener cal) {
    	btnNewButton_5.addActionListener(cal);
    }
	
	public String getId() {
        return textField.getText();
    }
	
	
	public String getAuthor() {
        return textField_2.getText();
    }
	
	public String getTitle() {
        return textField_1.getText();
    }
	
	public String getType() {
        return textField_3.getText();
    }
	
	public String getQuant() {
        return textField_4.getText();
    }
	
	public String getPrice() {
        return textField_5.getText();
    }
	

	
	public void setId(String newTotal) {
		textField.setText(newTotal);
    }
	
	public void setTitle(String newTotal) {
		textField_1.setText(newTotal);
    }
	
	public void setAuthor(String newTotal) {
		textField_2.setText(newTotal);
    }
	
	public void setType(String newTotal) {
		textField_3.setText(newTotal);
    }
	
	public void setQuant(String newTotal) {
		textField_4.setText(newTotal);
    }
	
	public void setPrice(String newTotal) {
		textField_5.setText(newTotal);
    }
	
	@Override
	public void update(Observable o, Object arg) {
		
		String[] columnNames = {"Book id","Title", "Author", "Type","Quantity","Price"};		
		Object[][] data= model.report();
		panel.setLayout(null);
		
		table = new JTable(data, columnNames);
		panel.setLayout(null);
		panel.setLayout(new BorderLayout());
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		
		repaint();
		
	}

}
