package view;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JTabbedPane;

import model.BookstoreModel;
import controller.EmployeeController;
import controller.BookController;


public class FrameAdmin {
private JFrame frame;
	
	
	public FrameAdmin(BookstoreModel mod)
	{
	frame = new JFrame();
	frame.setBounds(100, 100, 900, 500);
	frame.setTitle("Bookstore");
	frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	frame.setVisible(true);
	


	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	frame.getContentPane().add(tabbedPane);
	tabbedPane.setBounds(0, 0, 800, 800);

	JLayeredPane panel = new PanelBooks(mod);
	panel.setBounds(0, 0, 577, 500);
	BookController bc = new BookController(mod,(PanelBooks) panel);
	
	panel.setLayout(null);
	tabbedPane.addTab("Books", null, panel, null);
	
	
	JLayeredPane panel1 = new PanelEmployee(mod);
	panel1.setBounds(0, 0, 577, 500);
	EmployeeController ac = new EmployeeController(mod,(PanelEmployee) panel1);
	
	panel1.setLayout(null);
	tabbedPane.addTab("Employees", null, panel1, null);
	
	}


	
}
