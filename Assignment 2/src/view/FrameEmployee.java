package view;


import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JTabbedPane;

import controller.SearchController;



import model.BookstoreModel;



public class FrameEmployee {
	
	
	private JFrame frame;
	
	
	public FrameEmployee(BookstoreModel mod)
	{
	frame = new JFrame();
	frame.setBounds(100, 100, 900, 500);
	frame.setTitle("Bookstore");
	frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	frame.setVisible(true);
	

	
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	frame.getContentPane().add(tabbedPane);
	tabbedPane.setBounds(0, 0, 800, 500);

	JLayeredPane panel = new PanelSearch(mod);
	panel.setBounds(0, 0, 577, 300);
	SearchController sc = new SearchController(mod,(PanelSearch) panel);
	
	panel.setLayout(null);
	tabbedPane.addTab("Books", null, panel, null);
	}


	
}
