package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import model.BookstoreModel;



public class PanelLogin extends JFrame {

	private JFrame frame;
	private JPasswordField passwordFieldUser, passwordFieldAdmin;
	private JComboBox comboBox;
	private BookstoreModel model;

	public PanelLogin(BookstoreModel mod) {
		frame = new JFrame();
		frame.setTitle("Start");
		frame.setBounds(100, 100, 450, 330);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		model=mod;
		
		JLayeredPane panel = new JLayeredPane();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 450, 300);
		label.setIcon(img);
		panel.add(label, -1, 0);

		JButton btnAdmin = new JButton("Login as admin");
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				char[] input = passwordFieldAdmin.getPassword();
				if (isAdminPasswordCorrect(input)) {

					FrameAdmin f = new FrameAdmin(model);
				} else {
					JOptionPane.showMessageDialog(null, "Login unsuccessful");
				}

			}
		});
		btnAdmin.setBounds(110, 205, 159, 52);
		panel.add(btnAdmin);

		
		String[] emplNames = model.getAllEmployees();
		
		JButton btnUser = new JButton("Login as user");
		btnUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String selected = (String) comboBox.getSelectedItem();
				String a = new String (passwordFieldUser.getPassword());
				
				
				if (model.checkPass(selected, a)) {

					FrameEmployee f = new FrameEmployee(model);
				} else {
					JOptionPane.showMessageDialog(null, "Login unsuccessful");
				}
				

			}
		});
		btnUser.setBounds(110, 104, 159, 52);
		panel.add(btnUser);
		repaint();

		passwordFieldAdmin = new JPasswordField();
		passwordFieldAdmin.setBounds(184, 170, 140, 20);
		panel.add(passwordFieldAdmin);
		
		passwordFieldUser = new JPasswordField();
		passwordFieldUser.setBounds(184, 65, 140, 20);
		panel.add(passwordFieldUser);

		JLabel lblName = new JLabel("Name:");
		//lblName.setForeground(Color.white);
		lblName.setBounds(80, 32, 80, 14);
		panel.add(lblName);
		
		JLabel lblAdminPass = new JLabel("Password:");
		//lblAdminPass.setForeground(Color.white);
		lblAdminPass.setBounds(80, 67, 80, 14);
		panel.add(lblAdminPass);
		
		JLabel lblUserPass = new JLabel("Admin Password:");
		//lblUserPass.setForeground(Color.white);
		lblUserPass.setBounds(80, 173, 100, 14);
		panel.add(lblUserPass); 
		
		comboBox = new JComboBox(emplNames);
		comboBox.setBounds(184, 30, 140, 20);
		panel.add(comboBox);
		
		
		
		frame.setVisible(true);

	}

	private static boolean isAdminPasswordCorrect(char[] input) {
		boolean isCorrect = true;
		char[] correctPassword = { 'a', 'd', 'm', 'i', 'n' };

		if (input.length != correctPassword.length) {
			isCorrect = false;
		} else {
			isCorrect = Arrays.equals(input, correctPassword);
		}

		// Zero out the password.
		Arrays.fill(correctPassword, '0');

		return isCorrect;
	}

}
