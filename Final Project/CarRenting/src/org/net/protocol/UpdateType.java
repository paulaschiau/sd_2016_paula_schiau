package org.net.protocol;

public enum UpdateType {
	NEW_USER, DELETED_USER, UPDATED_USER,
	NEW_BOOKING, DELETED_BOOKING, UPDATED_BOOKING,
	NEW_CLIENT, UPDATED_CLIENT, DELETED_CLIENT
}
