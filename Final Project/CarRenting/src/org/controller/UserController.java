/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.model.Booking;
import org.model.Client;
import org.model.ClientSocket;
import org.model.LoginModel;
import org.model.UserModel;
import org.model.update.UserUpdateModel;
import org.view.LoginDialog;
import org.view.UserView;

/**
 *
 * @author Paula
 */
public class UserController {

	private static UserView userView;
	private static UserModel userModel;

	public UserController(UserView userView, UserModel userModel) {
		UserController.userView = userView;
		UserController.userModel = userModel;

		UserController.userView.addUserWindowListener(new UserWindowListener(UserController.userModel, UserController.userView));
		UserController.userView.addTableClientsTabListener(new TableClientsTabFocusEditListener());
		UserController.userView.addTableBookingsTabListener(new TableBookingsTabFocusEditListener());
		UserController.userView.addMenuLogoutListener(new MenuLogoutListener(UserController.userView));
		UserController.userView.addMenuExitListener(new MenuExitListener());
		UserController.userView.addCreateClientButtonListener(new CreateClientListener());
		UserController.userView.addDeleteClientButtonListener(new DeleteClientListener());
		UserController.userView.addCreateBookingButtonListener(new CreateBookingListener());
		UserController.userView.addDeleteBookingButtonListener(new DeleteBookingListener());
		UserController.userView.addComboDateListener(new DateInputListener());
	}

	private static class UserWindowListener extends WindowAdapter {
		private UserModel userModel;
		private UserView userView;
		private Timer updateTask;

		public UserWindowListener(UserModel userModel, UserView userView) {
			this.userModel = userModel;
			this.userView = userView;
		}

		@Override
		public void windowClosing(WindowEvent e) {
			updateTask.cancel();
			userModel.closeConnection();
			System.exit(0);
		}

		@Override
		public void windowOpened(WindowEvent e) {
			UserUpdateModel userUpdate = new UserUpdateModel(userView, userModel);
			updateTask = new Timer();
			updateTask.schedule(userUpdate, 0, 5000);
		}

	}

	private static class MenuExitListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			userModel.closeConnection();
			System.exit(0);
		}

	}

	private static class CreateClientListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Client client = userView.getCreateClientInput();
			userModel.createClient(client);
		}

	}

	private static class DeleteClientListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Client client = userView.getClientsTabSelectedClient();
			userModel.deleteClient(client);
		}

	}

	private static class MenuLogoutListener implements ActionListener {
		private UserView userView;

		public MenuLogoutListener(UserView userView) {
			this.userView = userView;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			userView.dispose();
			LoginDialog loginDialog = new LoginDialog();
			LoginModel loginModel = new LoginModel(ClientSocket.getConnection());
			loginDialog.openDialog();
			new LoginController(loginDialog, loginModel);
		}

	}

	private static class CreateBookingListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!userView.getTextInfo().equals("Date not valid!")) {
				Booking booking = userView.getCreateBookingInput();
				Client client = new Client();
				client.setClientID(userView.getClientsTabSelectedClient().getClientID());
				double price = userView.getPrice();
				booking.setClient(client);
				booking.setPrice(price);
				userModel.createBooking(booking);
			}
		}

	}

	private static class DeleteBookingListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (userView.isBookingTabSelected()) {
				Booking booking = userView.getBookingsTabSelectedBooking();
				userModel.deleteBooking(booking);
			} else if (userView.isClientTabSelected()) {
				Booking booking = userView.getClientsTabSelectedBooking();
				userModel.deleteBooking(booking);
			}
		}

	}

	private static class DateInputListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Booking booking = userView.getCreateBookingInput();
			if (userModel.validBooking(booking)) {
				userView.setTextInfo("");
				Client client = userView.getClientsTabSelectedClient();
				if (client != null) {
					client.setBookings(userModel.getBookings(client));
					StringBuilder price = new StringBuilder();
					price.append("Price: ");
					NumberFormat num = NumberFormat.getInstance();
					num.setMaximumFractionDigits(2);
					num.setMinimumFractionDigits(2);
					double pr = userModel.calculatePrice(client.getClientID(), booking);
					price.append(num.format(pr));
					userView.setPrice(pr);
					price.append("€/Day");
					userView.setTextInfo(price.toString());
				}
				userView.setAvailableCars(userModel.getAllAvailableCars(booking));
			} else {
				userView.setTextInfo("Date not valid!");
			}
		}

	}

	public static class TableClientsTabFocusEditListener implements TableModelListener, ListSelectionListener {

		private Client client;

		@Override
		public void tableChanged(TableModelEvent e) {
			if (e.getType() == TableModelEvent.UPDATE) {
				Client newClient = userView.getClientsTabSelectedClient();
				userModel.updateClient(client, newClient);
			}
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			ListSelectionModel lsm = (ListSelectionModel) e.getSource();
			if (!lsm.isSelectionEmpty()) {
				client = UserController.userView.getClientsTabSelectedClient();
				client.setBookings(UserController.userModel.getBookings(client));
				StringBuilder price = new StringBuilder();
				price.append("Price: ");
				NumberFormat num = NumberFormat.getInstance();
				num.setMaximumFractionDigits(2);
				num.setMinimumFractionDigits(2);
				double pr = userModel.calculatePrice(client.getClientID(), userView.getCreateBookingInput());
				price.append(num.format(pr));
				price.append("€/Day");
				userView.setPrice(pr);
				userView.setTextInfo(price.toString());
				userView.displayClientsTabBookings(client.getBookings());
			}
		}

	}

	public static class TableBookingsTabFocusEditListener implements TableModelListener, ListSelectionListener {

		private Booking booking;
		private Client client;

		@Override
		public void tableChanged(TableModelEvent e) {
			if (e.getType() == TableModelEvent.UPDATE) {
				Booking newBooking = userView.getBookingsTabSelectedBooking();
				userModel.updateBooking(booking, newBooking);
			}
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			ListSelectionModel lsm = (ListSelectionModel) e.getSource();
			if (!lsm.isSelectionEmpty()) {
				booking = UserController.userView.getBookingsTabSelectedBooking();
				List<Client> clients = new ArrayList<>();
				client = userModel.getClient(booking.getClient().getClientID());
				clients.add(client);
				userView.displayBookingsTabClients(clients);
			}
		}

	}

}
