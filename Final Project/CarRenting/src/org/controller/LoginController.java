package org.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import org.view.LoginDialog;
import org.model.LoginModel;
import org.model.User;

public class LoginController {
	private LoginDialog login;
	private LoginModel loginModel;

	public LoginController(LoginDialog login, LoginModel loginModel) {
		this.login = login;
		this.loginModel = loginModel;
		this.login.addOKListener(new OKListener());
		this.login.addCancelListener(new CancelListener());
		this.login.addWindowCloseListener(new WindowClose());
	}

	private class OKListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			User user = new User();
			user.setUsername(login.getUsername());
			user.setPassword(login.getPassword());
			loginModel.checkLogin(user, login);
		}

	}

	private class CancelListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				loginModel.closeConnection();
			} catch (Exception e1) {
				System.out.println(e1.getMessage());
			}
			System.exit(0);
		}
	}

	private class WindowClose extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent a) {
			try {
				loginModel.closeConnection();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			System.exit(0);
		}
	}
}
