package org.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Date;
import java.util.Timer;

import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.model.AdminModel;
import org.model.ClientSocket;
import org.model.LoginModel;
import org.model.User;
import org.model.update.AdminUpdateModel;
import org.view.AdminView;
import org.view.LoginDialog;

public class AdminController {

	private AdminView adminView;
	private AdminModel adminModel;

	public AdminController(AdminView adminView, AdminModel adminModel) {
		this.adminView = adminView;
		this.adminModel = adminModel;
		this.adminView.addWindowListener(new CustomWindowListener(this.adminView, this.adminModel));
		this.adminView.addUsersTableListener(new UsersTableListener(this.adminView, this.adminModel));
		this.adminView.addCreateListener(new CreateListener());
		this.adminView.addDeleteListener(new DeleteListener());
		this.adminView.addReportListener(new ReportListener());
		this.adminView.addExitMenuListener(new ExitMenuListener(this.adminModel));
		this.adminView.addLogoutMenuListener(new LogoutMenuListener(this.adminView));
	}

	private class CustomWindowListener extends WindowAdapter {
		private AdminView adminView;
		private AdminModel adminModel;
		private Timer updateTask;

		public CustomWindowListener(AdminView adminView, AdminModel adminModel) {
			this.adminView = adminView;
			this.adminModel = adminModel;
		}

		@Override
		public void windowOpened(WindowEvent e) {
			AdminUpdateModel adminUpdate = new AdminUpdateModel(adminView, adminModel);
			updateTask = new Timer();
			updateTask.schedule(adminUpdate, 0, 5000);
		}

		@Override
		public void windowClosing(WindowEvent e) {
			updateTask.cancel();
			adminModel.closeConnection();
			System.exit(0);
		}

	}

	private class ExitMenuListener implements ActionListener {
		private AdminModel adminModel;

		public ExitMenuListener(AdminModel adminModel) {
			this.adminModel = adminModel;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			adminModel.closeConnection();
			System.exit(0);
		}
	}

	private class LogoutMenuListener implements ActionListener {
		private AdminView adminView;

		public LogoutMenuListener(AdminView adminView) {
			this.adminView = adminView;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			adminView.dispose();
			LoginDialog loginView = new LoginDialog();
			LoginModel loginModel = new LoginModel(ClientSocket.getConnection());
			loginView.openDialog();
			new LoginController(loginView, loginModel);
		}

	}

	public static class UsersTableListener implements TableModelListener, ListSelectionListener {

		private User user;
		private AdminView adminView;
		private AdminModel adminModel;

		public UsersTableListener(AdminView adminView, AdminModel adminModel) {
			this.adminView = adminView;
			this.adminModel = adminModel;
		}

		@Override
		public void tableChanged(TableModelEvent e) {
			if (e.getType() == TableModelEvent.UPDATE) {
				User newUser = adminView.getSelectedUser();
				adminModel.updateUser(user, newUser);
			}
		}

		@Override
		public void valueChanged(ListSelectionEvent e) {
			ListSelectionModel lsm = (ListSelectionModel) e.getSource();
			if (!lsm.isSelectionEmpty()) {
				user = adminView.getSelectedUser();
			}
		}

	}

	private class CreateListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			User user = adminView.getCreateUserInput();
			adminModel.createUser(user);
		}

	}

	private class DeleteListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			User user = adminView.getSelectedUser();
			adminModel.deleteUser(user);
		}
	}

	private class ReportListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Date fromDate = adminView.getReporFromDate();
			Date toDate = adminView.getReportToDate();
			JFileChooser saveDlg = new JFileChooser();
			FileFilter txtFilter = new FileNameExtensionFilter(".txt", "txt");
			FileFilter xmlFile = new FileNameExtensionFilter(".xml", "xml");
			FileFilter def = saveDlg.getFileFilter();
			saveDlg.removeChoosableFileFilter(def);
			saveDlg.addChoosableFileFilter(txtFilter);
			saveDlg.addChoosableFileFilter(xmlFile);
			if (saveDlg.showSaveDialog(adminView) == JFileChooser.APPROVE_OPTION) {
				File file = saveDlg.getSelectedFile();
				StringBuilder filePath = new StringBuilder();
				filePath.append(file.getAbsolutePath());
				filePath.append(saveDlg.getFileFilter().getDescription());
				adminModel.createReport(saveDlg.getFileFilter().getDescription(), fromDate, toDate, filePath.toString());
			}
		}
	}
}
