/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.view;

import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import javax.swing.table.DefaultTableModel;
import org.model.AdminModel;
import org.model.User;

/**
 *
 * @author Paula
 */
public class AdminView extends javax.swing.JFrame implements Observer {

	private static final long serialVersionUID = 100L;
	private static final String DATE_FORMAT = "dd-MMM-yyyy";
	private AdminModel adminModel;

	public AdminView(AdminModel adminModel) {
		this.adminModel = adminModel;
		adminModel.addObserver(this);
		initComponents();
		displayUsers(this.adminModel.getAllUsers());
	}

	public void displayUsers(List<User> users) {
		DefaultTableModel usersTable = (DefaultTableModel) jTableUsers.getModel();
		usersTable.setRowCount(0);
		for (User user : users) {
			String name = user.getName();
			String username = user.getUsername();
			String password = user.getPassword();
			usersTable.addRow(new Object[] { name, username, password });
		}
	}

	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	// <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableUsers = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextName = new javax.swing.JTextField();
        jTextUsername = new javax.swing.JTextField();
        jTextPassword = new javax.swing.JTextField();
        jButtonCreateUser = new javax.swing.JButton();
        jButtonDeleteUser = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jComboFromDay = new javax.swing.JComboBox<Integer>();
        jComboFromMonth = new javax.swing.JComboBox<String>();
        jComboFromYear = new javax.swing.JComboBox<Integer>();
        jLabel6 = new javax.swing.JLabel();
        jComboToDay = new javax.swing.JComboBox<Integer>();
        jComboToMonth = new javax.swing.JComboBox<String>();
        jComboToYear = new javax.swing.JComboBox<Integer>();
        jButtonGenerateReport = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuExit = new javax.swing.JMenuItem();
        jMenuLogout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Administrator");
        setResizable(false);

        jTableUsers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "username", "password"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTableUsers);

        jLabel1.setText("User accounts:");

        jLabel2.setText("Name:");

        jLabel3.setText("username:");

        jLabel4.setText("password:");

        jButtonCreateUser.setText("Create");

        jButtonDeleteUser.setText("Delete");

        jLabel5.setText("From:");

        jComboFromDay.setModel(new javax.swing.DefaultComboBoxModel(new Integer[] { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 }));

        jComboFromMonth.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }));

        jComboFromYear.setModel(new javax.swing.DefaultComboBoxModel(new Integer[] { 2010,2011,2012,2013,2014,2015,2016,2017 }));

        jLabel6.setText("To:");

        jComboToDay.setModel(new javax.swing.DefaultComboBoxModel(new Integer[] { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 }));
        jComboToDay.setSelectedIndex(1);

        jComboToMonth.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" }));

        jComboToYear.setModel(new javax.swing.DefaultComboBoxModel(new Integer[] { 2010,2011,2012,2013,2014,2015,2016,2017 }));

        jButtonGenerateReport.setText("Generate Report");

        jMenu1.setText("File");
        
        jMenuLogout.setText("Logout");
        jMenu1.add(jMenuLogout);

        jMenuExit.setText("Exit");
        jMenu1.add(jMenuExit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addGap(26, 26, 26)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextName, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                    .addComponent(jTextUsername)
                                    .addComponent(jTextPassword))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButtonCreateUser)
                                    .addComponent(jButtonDeleteUser))
                                .addGap(74, 74, 74))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jSeparator1)
                                .addContainerGap())
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jComboToDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jComboToMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jComboToYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel5)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jComboFromDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jComboFromMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(jComboFromYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addContainerGap(278, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonGenerateReport)
                                .addGap(89, 89, 89))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(jTextName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(63, 63, 63)
                                .addComponent(jButtonCreateUser)))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jTextUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addComponent(jButtonDeleteUser)
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jTextPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(72, 72, 72)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jComboFromDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jComboFromMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jComboFromYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(42, 42, 42)
                                .addComponent(jLabel6))
                            .addComponent(jButtonGenerateReport))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboToDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboToMonth, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboToYear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args
	 *            the command line arguments
	 */
	public void openWindow() {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("System".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(AdminView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(AdminView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(AdminView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(AdminView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>
		this.setVisible(true);
	}

	public void closeWindow() {
		this.setVisible(false);
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCreateUser;
    private javax.swing.JButton jButtonDeleteUser;
    private javax.swing.JButton jButtonGenerateReport;
    private javax.swing.JComboBox<Integer> jComboFromDay;
    private javax.swing.JComboBox<String> jComboFromMonth;
    private javax.swing.JComboBox<Integer> jComboFromYear;
    private javax.swing.JComboBox<Integer> jComboToDay;
    private javax.swing.JComboBox<String> jComboToMonth;
    private javax.swing.JComboBox<Integer> jComboToYear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuExit;
    private javax.swing.JMenuItem jMenuLogout;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTableUsers;
    private javax.swing.JTextField jTextName;
    private javax.swing.JTextField jTextPassword;
    private javax.swing.JTextField jTextUsername;
    // End of variables declaration//GEN-END:variables

	public void addNewWindowListener(WindowListener a) {
		this.addWindowListener(a);
	}

	public void addUsersTableListener(org.controller.AdminController.UsersTableListener a) {
		jTableUsers.getModel().addTableModelListener(a);
		jTableUsers.getSelectionModel().addListSelectionListener(a);
	}

	public void addCreateListener(ActionListener a) {
		jButtonCreateUser.addActionListener(a);
	}

	public void addDeleteListener(ActionListener a) {
		jButtonDeleteUser.addActionListener(a);
	}

	public void addReportListener(ActionListener a) {
		jButtonGenerateReport.addActionListener(a);
	}
	
	public void addExitMenuListener(ActionListener a){
		jMenuExit.addActionListener(a);
	}
	
	public void addLogoutMenuListener(ActionListener a){
		jMenuLogout.addActionListener(a);
	}

	public User getSelectedUser() {
		int i = jTableUsers.getSelectedRow();
		String name = (String) jTableUsers.getValueAt(i, 0);
		String username = (String) jTableUsers.getValueAt(i, 1);
		String password = (String) jTableUsers.getValueAt(i, 2);
		User user = new User();
		user.setName(name);
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}

	public User getCreateUserInput() {
		User user = new User();
		user.setName(jTextName.getText());
		user.setUsername(jTextUsername.getText());
		user.setPassword(jTextPassword.getText());
		user.setType("user");
		return user;
	}

	public Date getReporFromDate() {
		DateFormat df = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
		int day = (int) jComboFromDay.getSelectedItem();
		String month = (String) jComboFromMonth.getSelectedItem();
		int year = (int) jComboFromYear.getSelectedItem();
		StringBuilder date = new StringBuilder();
		date.append(day);
		date.append("-");
		date.append(month);
		date.append("-");
		date.append(year);
		try {
			return df.parse(date.toString());
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	public Date getReportToDate() {
		DateFormat df = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
		int day = (int) jComboToDay.getSelectedItem();
		String month = (String) jComboToMonth.getSelectedItem();
		int year = (int) jComboToYear.getSelectedItem();
		StringBuilder date = new StringBuilder();
		date.append(day);
		date.append("-");
		date.append(month);
		date.append("-");
		date.append(year);
		try {
			return df.parse(date.toString());
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		List<User> users = adminModel.getAllUsers();
		displayUsers(users);
	}
}
