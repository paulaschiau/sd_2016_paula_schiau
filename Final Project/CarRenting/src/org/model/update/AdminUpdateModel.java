package org.model.update;

import java.util.TimerTask;

import org.model.AdminModel;
import org.model.ClientSocket;
import org.net.protocol.Command;
import org.net.protocol.CommandType;
import org.net.protocol.Response;
import org.net.protocol.UpdateType;
import org.view.AdminView;

public class AdminUpdateModel extends TimerTask {

	private AdminView adminView;
	private AdminModel adminModel;
	private Command command;

	public AdminUpdateModel(AdminView adminView, AdminModel adminModel) {
		this.adminView = adminView;
		this.adminModel = adminModel;
		command = new Command();
		command.setCommandType(CommandType.CHECK_UPDATE);
	}

	@Override
	public void run() {
		Response response = ClientSocket.getConnection().executeCommand(command);
		if (response != null) {
			switch (UpdateType.valueOf(response.getInfo())) {
			case DELETED_USER:
				adminView.displayUsers(adminModel.getAllUsers());
				break;
			case NEW_USER:
				adminView.displayUsers(adminModel.getAllUsers());
				break;
			case UPDATED_USER:
				adminView.displayUsers(adminModel.getAllUsers());
				break;
			default:
				break;
			}
		}
	}

}
