package org.model.update;

import java.util.TimerTask;

import org.model.ClientSocket;
import org.model.UserModel;
import org.net.protocol.Command;
import org.net.protocol.CommandType;
import org.net.protocol.Response;
import org.net.protocol.UpdateType;
import org.view.UserView;

public class UserUpdateModel extends TimerTask {

	private UserView userView;
	private UserModel userModel;
	private Command command;

	public UserUpdateModel(UserView userView, UserModel userModel) {
		this.userView = userView;
		this.userModel = userModel;
		command = new Command();
		command.setCommandType(CommandType.CHECK_UPDATE);
	}

	@Override
	public void run() {
		Response response = ClientSocket.getConnection().executeCommand(command);
		if (response != null) {
			switch (UpdateType.valueOf(response.getInfo())) {
			case DELETED_BOOKING:
				userView.displayBookingsTabBookings(userModel.getAllBookings());
				break;
			case DELETED_CLIENT:
				userView.displayClientsTabClients(userModel.getAllClients());
				break;
			case NEW_BOOKING:
				userView.displayBookingsTabBookings(userModel.getAllBookings());
				break;
			case NEW_CLIENT:
				userView.displayClientsTabClients(userModel.getAllClients());
				break;
			case UPDATED_BOOKING:
				userView.displayBookingsTabBookings(userModel.getAllBookings());
				break;
			case UPDATED_CLIENT:
				userView.displayClientsTabClients(userModel.getAllClients());
				break;
			default:
				break;
			}
		}
	}

}
