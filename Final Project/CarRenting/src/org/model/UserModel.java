/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import org.net.protocol.Command;
import org.net.protocol.CommandType;
import org.net.protocol.Response;

/**
 *
 * @author Paul
 */
public class UserModel extends Observable {

	public List<Booking> getBookings(Client client) {
		List<Booking> bookings = new ArrayList<>();
		Command command = new Command();
		command.setCommandType(CommandType.READ_BOOKINGS_BY_CLIENT);
		List<Object> data = new ArrayList<>();
		data.add(client);
		command.setCommandData(data);
		Response response = ClientSocket.getConnection().executeCommand(command);
		for (Object obj : response.getResponseData()) {
			bookings.add((Booking) obj);
		}
		return bookings;
	}

	public List<Client> getAllClients() {
		List<Client> clients = new ArrayList<>();
		Command command = new Command();
		command.setCommandType(CommandType.READ_CLIENTS);
		Response response = ClientSocket.getConnection().executeCommand(command);
		for (Object obj : response.getResponseData()) {
			clients.add((Client) obj);
		}
		return clients;
	}

	public Client getClient(int clientID) {
		Command command = new Command();
		command.setCommandType(CommandType.READ_CLIENT_BY_ID);
		List<Object> clients = new ArrayList<>();
		clients.add(clientID);
		command.setCommandData(clients);
		Response response = ClientSocket.getConnection().executeCommand(command);
		return (Client) response.getResponseData().get(0);
	}

	public List<Booking> getAllBookings() {
		Command command = new Command();
		command.setCommandType(CommandType.READ_BOOKINGS);
		Response response = ClientSocket.getConnection().executeCommand(command);
		List<Booking> bookings = new ArrayList<>();
		for (Object obj : response.getResponseData()) {
			bookings.add((Booking) obj);
		}
		return bookings;
	}

	public List<Car> getAllAvailableCars(Booking booking) {
		Command command = new Command();
		command.setCommandType(CommandType.CHECK_AVAILABLE_CARS);
		List<Object> dates = new ArrayList<>();
		dates.add(booking.getFromDate());
		dates.add(booking.getToDate());
		command.setCommandData(dates);
		Response response = ClientSocket.getConnection().executeCommand(command);
		List<Car> cars = new ArrayList<>();
		for (Object obj : response.getResponseData()) {
			cars.add((Car) obj);
		}
		return cars;
	}

	public void createClient(Client client) {
		Command command = new Command();
		command.setCommandType(CommandType.CREATE_CLIENT);
		List<Object> obj = new ArrayList<>();
		obj.add(client);
		command.setCommandData(obj);
		Response response = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public boolean validBooking(Booking booking) {
		return booking.getToDate().getTime() > booking.getFromDate().getTime();
	}

	public double calculatePrice(int clientID, Booking booking) {
		Command command = new Command();
		command.setCommandType(CommandType.CALCULATE_PRICE);
		List<Object> obj = new ArrayList<>();
		obj.add(clientID);
		obj.add(booking);
		command.setCommandData(obj);
		Response response = ClientSocket.getConnection().executeCommand(command);
		return (double) response.getResponseData().get(0);
	}

	public void deleteClient(Client client) {
		Command command = new Command();
		command.setCommandType(CommandType.DELETE_CLIENT);
		List<Object> obj = new ArrayList<>();
		obj.add(client);
		command.setCommandData(obj);
		Response response = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public void updateClient(Client oldClient, Client newClient) {
		Command command = new Command();
		command.setCommandType(CommandType.UPDATE_CLIENT);
		List<Object> objC = new ArrayList<>();
		objC.add(oldClient);
		objC.add(newClient);
		command.setCommandData(objC);
		Response res = ClientSocket.getConnection().executeCommand(command);
		if (oldClient.getBookings() != null && oldClient.getClientID() != newClient.getClientID()) {
			for (Booking booking : oldClient.getBookings()) {
				Booking newBooking = new Booking();
				Client client = new Client();
				client.setClientID(newClient.getClientID());
				newBooking.copy(booking);
				newBooking.setClient(client);
				command = new Command();
				command.setCommandType(CommandType.UPDATE_BOOKING);
				List<Object> objB = new ArrayList<>();
				objB.add(booking);
				objB.add(newBooking);
				command.setCommandData(objB);
				Response resp = ClientSocket.getConnection().executeCommand(command);
			}
		}
		setChanged();
		notifyObservers();
	}

	public void createBooking(Booking booking) {
		Command command = new Command();
		command.setCommandType(CommandType.CREATE_BOOKING);
		List<Object> obj = new ArrayList<>();
		obj.add(booking);
		command.setCommandData(obj);
		Response res = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public void deleteBooking(Booking booking) {
		Command command = new Command();
		command.setCommandType(CommandType.DELETE_BOOKING);
		List<Object> obj = new ArrayList<>();
		obj.add(booking);
		command.setCommandData(obj);
		Response res = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public void updateBooking(Booking oldBooking, Booking newBooking) {
		Command command = new Command();
		command.setCommandType(CommandType.UPDATE_BOOKING);
		List<Object> obj = new ArrayList<>();
		obj.add(oldBooking);
		obj.add(newBooking);
		command.setCommandData(obj);
		Response res = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public void closeConnection() {
		Command command = new Command();
		command.setCommandType(CommandType.FINISH);
		ClientSocket.getConnection().executeCommand(command);
		ClientSocket.getConnection().closeConnection();
	}

}
