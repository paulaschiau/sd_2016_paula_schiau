package org.model;

import java.util.ArrayList;
import java.util.List;

import org.controller.AdminController;
import org.controller.LoginController;
import org.controller.UserController;
import org.net.protocol.Command;
import org.net.protocol.CommandType;
import org.net.protocol.Response;
import org.view.AdminView;
import org.view.LoginDialog;
import org.view.UserView;

public class LoginModel {
	private ClientSocket clientSocket;

	public LoginModel(ClientSocket connection) {
		this.clientSocket = connection;
	}

	public void checkLogin(User user, LoginDialog login) {
		Command command = new Command();
		command.setCommandType(CommandType.LOGIN);
		List<Object> users = new ArrayList<>();
		users.add(user);
		command.setCommandData(users);
		Response response = clientSocket.executeCommand(command);
		if (response.getInfo().equals("user")) {
			UserModel userModel = new UserModel();
			UserView userView = new UserView(userModel);
			new UserController(userView, userModel);
			login.closeDialog();
			userView.openWindow();
		} else if (response.getInfo().equals("admin")) {
			AdminModel adminModel = new AdminModel();
			AdminView adminView = new AdminView(adminModel);
			new AdminController(adminView, adminModel);
			login.closeDialog();
			adminView.openWindow();
		} else {
			System.out.println("Unrecognized user!");
		}
	}

	public void closeConnection() {
		Command command = new Command();
		command.setCommandType(CommandType.FINISH);
		clientSocket.executeCommand(command);
		clientSocket.closeConnection();
	}

	public static void main(String args[]) {
		LoginDialog login = new LoginDialog();
		LoginModel loginModel = new LoginModel(ClientSocket.getConnection());
		new LoginController(login, loginModel);
		login.openDialog();
	}

}
