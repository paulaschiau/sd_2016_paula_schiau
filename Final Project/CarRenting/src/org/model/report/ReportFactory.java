/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.report;

/**
 *
 * @author Paula
 */
public class ReportFactory {

	public Report getReport(String type) {
		if (type.equals(".txt")) {
			return new TextReport();
		} else if (type.equals(".xml")) {
			return new XmlReport();
		}
		return null;
	}
}
