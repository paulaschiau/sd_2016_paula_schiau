/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.report;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;
import org.model.Booking;
import org.model.Client;

/**
 *
 * @author Paula
 */
public class TextReport implements Report {

	@Override
	public void createReport(List<Booking> bookings, List<Client> clients, String filePath) {
		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "utf-8"));
			StringBuilder output;
			writer.write("Bookings report:\r\n\r\n");
			for (Booking booking : bookings) {
				output = new StringBuilder();
				output.append("Booking of car: ");
				output.append(booking.getCar().getCarNr());
				output.append("\r\nfrom: ");
				output.append(booking.getFromDate().toString());
				output.append(" to: ");
				output.append(booking.getToDate().toString());
				output.append("\r\nClient who booked the car: First Name: ");
				output.append(booking.getClient().getFirstName());
				output.append(" Last Name: ");
				output.append(booking.getClient().getLastName());
				output.append("\r\n\r\n");
				writer.write(output.toString());
			}
			writer.write("Clients report:\r\n\r\n");
			for (Client client : clients) {
				output = new StringBuilder();
				output.append("Client: First Name: ");
				output.append(client.getFirstName());
				output.append(" Last Name: ");
				output.append(client.getLastName());
				output.append("\r\n");
				for (Booking booking : client.getBookings()) {
					output.append("Booked car nr: ");
					output.append(booking.getCar().getCarNr());
					output.append(" from: ");
					output.append(booking.getFromDate().toString());
					output.append(" to: ");
					output.append(booking.getToDate().toString());
					output.append("\r\n");
				}
				output.append("\r\n");
				writer.write(output.toString());
			}
			writer.close();
		} catch (UnsupportedEncodingException | FileNotFoundException ex) {
			// todo
		} catch (IOException ex) {
			// todo
		}
	}

}
