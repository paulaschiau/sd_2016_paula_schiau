/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.report;

import java.io.File;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.model.Booking;
import org.model.Client;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Paula
 */
public class XmlReport implements Report {

	@Override
	public void createReport(List<Booking> bookings, List<Client> clients, String filePath) {
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder;
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.newDocument();

			Element root = document.createElement("report");
			document.appendChild(root);
			for (Booking booking : bookings) {
				int roomNr = booking.getCar().getCarNr();
				Date fromDate = booking.getFromDate();
				Date toDate = booking.getToDate();
				String firstName = booking.getClient().getFirstName();
				String lastName = booking.getClient().getLastName();

				Element newBooking = document.createElement("booking");
				root.appendChild(newBooking);
				Element newCarNr = document.createElement("CarNr");
				newCarNr.appendChild(document.createTextNode(Integer.toString(roomNr)));
				newBooking.appendChild(newCarNr);
				Element newFromDate = document.createElement("from");
				newFromDate.appendChild(document.createTextNode(fromDate.toString()));
				newBooking.appendChild(newFromDate);
				Element newToDate = document.createElement("to");
				newToDate.appendChild(document.createTextNode(toDate.toString()));
				newBooking.appendChild(newToDate);
				Element newFirstName = document.createElement("firstName");
				newFirstName.appendChild(document.createTextNode(firstName));
				newBooking.appendChild(newFirstName);
				Element newLastName = document.createElement("lastName");
				newLastName.appendChild(document.createTextNode(lastName));
				newBooking.appendChild(newLastName);
			}
			for (Client client : clients) {
				String firstName = client.getFirstName();
				String lastName = client.getLastName();
				Element newClient = document.createElement("client");
				root.appendChild(newClient);
				Element clientFirstName = document.createElement("firstName");
				clientFirstName.appendChild(document.createTextNode(firstName));
				newClient.appendChild(clientFirstName);
				Element clientLastName = document.createElement("lastName");
				clientLastName.appendChild(document.createTextNode(lastName));
				newClient.appendChild(clientLastName);
			}
			try {
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource domSource = new DOMSource(document);
				StreamResult streamResult = new StreamResult(new File(filePath));
				transformer.transform(domSource, streamResult);
			} catch (TransformerException ex) {
				System.out.println("Failed to write to xml file!");
			}
		} catch (ParserConfigurationException ex) {
			System.out.println("Failed to create xml file!");
		}
	}

}
