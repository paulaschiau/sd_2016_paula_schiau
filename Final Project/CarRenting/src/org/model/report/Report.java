/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model.report;

import java.util.List;
import org.model.Client;
import org.model.Booking;

/**
 *
 * @author Paula
 */
public interface Report {
    public void createReport(List<Booking> bookings, List<Client> clients, String filePath);
}
