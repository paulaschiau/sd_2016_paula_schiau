package org.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Observable;

import org.model.report.Report;
import org.model.report.ReportFactory;
import org.net.protocol.Command;
import org.net.protocol.CommandType;
import org.net.protocol.Response;

/**
 *
 * @author Paula
 */
public class AdminModel extends Observable {
	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		Command command = new Command();
		command.setCommandType(CommandType.READ_USERS);
		Response response = ClientSocket.getConnection().executeCommand(command);
		for (Object obj : response.getResponseData()) {
			users.add((User) obj);
		}
		return users;
	}

	public void createUser(User user) {
		Command command = new Command();
		command.setCommandType(CommandType.CREATE_USER);
		List<Object> users = new ArrayList<>();
		users.add(user);
		command.setCommandData(users);
		Response resp = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public void updateUser(User oldUser, User newUser) {
		Command command = new Command();
		command.setCommandType(CommandType.UPDATE_USER);
		List<Object> users = new ArrayList<>();
		users.add(oldUser);
		users.add(newUser);
		command.setCommandData(users);
		Response res = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public void deleteUser(User user) {
		Command command = new Command();
		command.setCommandType(CommandType.DELETE_USER);
		List<Object> users = new ArrayList<>();
		users.add(user);
		command.setCommandData(users);
		Response res = ClientSocket.getConnection().executeCommand(command);
		setChanged();
		notifyObservers();
	}

	public void createReport(String type, Date fromDate, Date toDate, String filePath) {
		ReportFactory reportFactory = new ReportFactory();
		Report report = reportFactory.getReport(type);
		List<Booking> bookings = new ArrayList<>();
		List<Client> clients = new ArrayList<>();
		List<Object> obj = getReport(fromDate, toDate);
		int i, n, m;
		n = (int) obj.get(0);
		for (i = 1; i <= n; i++) {
			bookings.add((Booking) obj.get(i));
		}
		m = (int) obj.get(i);
		i++;
		for (; i < obj.size(); i++) {
			clients.add((Client) obj.get(i));
		}
		for (Booking booking : bookings) {
			Command command = new Command();
			command.setCommandType(CommandType.READ_CLIENT_BY_ID);
			obj = new ArrayList<>();
			obj.add(booking.getClient().getClientID());
			command.setCommandData(obj);
			Response res = ClientSocket.getConnection().executeCommand(command);
			Client client = (Client) res.getResponseData().get(0);
			booking.setClient(client);
			bookings.set(bookings.indexOf(booking), booking);
		}
		report.createReport(bookings, clients, filePath);
	}

	private List<Object> getReport(Date fromDate, Date toDate) {
		Command command = new Command();
		command.setCommandType(CommandType.REPORT);
		List<Object> obj = new ArrayList<>();
		obj.add(fromDate);
		obj.add(toDate);
		command.setCommandData(obj);
		Response response = ClientSocket.getConnection().executeCommand(command);
		return response.getResponseData();
	}

	public void closeConnection() {
		Command command = new Command();
		command.setCommandType(CommandType.FINISH);
		ClientSocket.getConnection().executeCommand(command);
		ClientSocket.getConnection().closeConnection();
	}
}
