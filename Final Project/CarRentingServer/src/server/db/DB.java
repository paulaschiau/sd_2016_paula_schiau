/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.db;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Paula
 */
public class DB {

    private static DB database = null;
    private static Connection conn = null;
    private static Statement stat = null;

    private final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private final static String JDBC_URL = "jdbc:mysql://localhost:3306/carrenting";

    private final static String userName = "root";
    private final static String userPassword = "root";

    private DB() {
        try {
            Class.forName(JDBC_DRIVER);
            try {
                conn = (Connection) DriverManager.getConnection(JDBC_URL, userName, userPassword);
                stat = (Statement) conn.createStatement();
            } catch (SQLException ex) {
                // TODO
            }
        } catch (ClassNotFoundException ex) {
            // TODO
        }
    }

    public synchronized void closeConnection() {
        try {
            conn.close();
        } catch (SQLException ex) {
            // TODO
            ex.printStackTrace();
        }
    }

    public static synchronized DB getDBInstance() {
        if (database == null) {
            database = new DB();
        }
        return database;
    }

    // update information in the database
    public synchronized void executeUpdate(String query) {
        try {
            stat.executeUpdate(query);
        } catch (SQLException ex) {
            // TODO
        }
    }

    // read information from database
    public synchronized ResultSet executeQuery(String query) {
        ResultSet res = null;
        try {
            res = stat.executeQuery(query);
        } catch (SQLException ex) {
            // TODO
        }
        return res;
    }
}
