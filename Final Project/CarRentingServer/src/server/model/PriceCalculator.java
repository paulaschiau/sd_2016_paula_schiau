package server.model;

import java.util.Calendar;
import java.util.List;

import org.model.Booking;
import org.model.Client;

import server.model.mappers.BookingMapper;

public class PriceCalculator {

	private static final int WINTER_MONTH1 = Calendar.DECEMBER;
	private static final int WINTER_MONTH2 = Calendar.JANUARY;
	private static final int SUMMER_MONTH1 = Calendar.JUNE;
	private static final int SUMMER_MONTH2 = Calendar.JULY;
	private static final int SUMMER_MONTH3 = Calendar.AUGUST;

	private static final float STANDARD_PRICE = (float) 24.9;
	private static final float WINTER_PRICE = (float) 29.9;
	private static final float SUMMER_PRICE = (float) 39.9;

	private static final double LOYALTY1 = 1.7;
	private static final double LOYALTY2 = 1.2;

	private boolean checkSeason(Calendar date, String season) {
		if (season.equalsIgnoreCase("summer")) {
			if (date.get(Calendar.MONTH) == SUMMER_MONTH1) {
				return true;
			}
			if (date.get(Calendar.MONTH) == SUMMER_MONTH2) {
				return true;
			}
			if (date.get(Calendar.MONTH) == SUMMER_MONTH3) {
				return true;
			}
		}
		if (season.equalsIgnoreCase("winter")) {
			if (date.get(Calendar.MONTH) == WINTER_MONTH1) {
				return true;
			}
			if (date.get(Calendar.MONTH) == WINTER_MONTH2) {
				return true;
			}
		}
		return false;
	}

	public double calculatePrice(int clientID, Booking booking) {
		double price = 0.0;
		Calendar date = Calendar.getInstance();
		date.setTime(booking.getFromDate());
		int days = (int) ((booking.getToDate().getTime() - booking.getFromDate().getTime()) / (86400000));
		days++;
		for (int i = 0; i < days; i++) {
			date.add(Calendar.DATE, i);
			if (this.checkSeason(date, "winter")) {
				price += WINTER_PRICE;
			} else if (this.checkSeason(date, "summer")) {
				price += SUMMER_PRICE;
			} else {
				price += STANDARD_PRICE;
			}
		}
		price /= (float) days;
		BookingMapper bookingMapper = new BookingMapper();
		Client client = new Client();
		client.setClientID(clientID);
		List<Booking> bookings = bookingMapper.read(client);
		int nrOfBookings = bookings.size();
		if (nrOfBookings >= 5) {
			price /= LOYALTY1;
		} else if (nrOfBookings >= 3) {
			price /= LOYALTY2;
		}
		return price;
	}

}
