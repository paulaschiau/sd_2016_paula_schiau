package server.model;

import java.util.Observable;
import java.util.Observer;

import org.net.protocol.UpdateType;

public class ServerObserver implements Observer {

	private static boolean isUpdate;
	private static UpdateType whatChanged;
	private static int nrOfClients;
	private static Server server;

	public ServerObserver(Server server) {
		ServerObserver.server = server;
	}

	@Override
	public void update(Observable o, Object arg) {
		ServerObserver.isUpdate = true;
		ServerObserver.whatChanged = (UpdateType) arg;
		ServerObserver.nrOfClients = ServerObserver.server.getClients().size();
	}

	public static UpdateType whatChanged() {
		return whatChanged;
	}

	public static boolean isUpdate() {
		return isUpdate;
	}

	public static void setUpdate(boolean isUpdate) {
		ServerObserver.isUpdate = isUpdate;
	}

	public static int getNrOfClients() {
		return nrOfClients;
	}

	public static void decreaseNrOfClients() {
		ServerObserver.nrOfClients--;
	}

}
