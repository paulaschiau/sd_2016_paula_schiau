/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.model.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.model.Client;

import server.db.DB;

/**
 *
 * @author Paula
 */
public class ClientMapper {

	public void create(Client client) {
		StringBuilder query = new StringBuilder();
		query.append("insert into clients (clientID,firstName,lastName,phoneNo) values ('");
		query.append(client.getClientID());
		query.append("','");
		query.append(client.getFirstName());
		query.append("','");
		query.append(client.getLastName());
		query.append("','");
		query.append(client.getPhoneNo());
		query.append("');");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public void update(Client oldClient, Client newClient) {
		StringBuilder query = new StringBuilder();
		query.append("update clients set clientID='");
		query.append(newClient.getClientID());
		query.append("',firstName='");
		query.append(newClient.getFirstName());
		query.append("',lastName='");
		query.append(newClient.getLastName());
		query.append("',phoneNo='");
		query.append(newClient.getPhoneNo());
		query.append("' where clientID='");
		query.append(oldClient.getClientID());
		query.append("';");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public List<Client> readAll() {
		List<Client> clients = new ArrayList<>();
		ResultSet res;
		res = DB.getDBInstance().executeQuery("select * from clients;");
		try {
			while (res.next()) {
				Client client = new Client();
				client.setClientID(res.getInt("clientID"));
				client.setFirstName(res.getString("firstName"));
				client.setLastName(res.getString("lastName"));
				client.setPhoneNo(res.getString("phoneNo"));
				clients.add(client);
			}
		} catch (SQLException e) {
			System.out.println("error in readAll client mapper");
		}
		return clients;
	}

	public Client read(int clientID) {
		StringBuilder query = new StringBuilder();
		query.append("select * from clients where clientID='");
		query.append(clientID);
		query.append("';");
		ResultSet res = DB.getDBInstance().executeQuery(query.toString());
		Client client = new Client();
		try {
			res.first();
			client.setClientID(clientID);
			client.setFirstName(res.getString("firstName"));
			client.setLastName(res.getString("lastName"));
			client.setPhoneNo(res.getString("phoneNo"));
		} catch (SQLException e) {
			// to do
		}
		return client;
	}

	public void delete(Client client) {
		StringBuilder query = new StringBuilder();
		query.append("delete from clients where clientID='");
		query.append(client.getClientID());
		query.append("';");
		DB.getDBInstance().executeUpdate(query.toString());
	}

}
