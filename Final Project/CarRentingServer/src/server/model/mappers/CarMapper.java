/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.model.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.model.Car;

import server.db.DB;

/**
 *
 * @author Paula
 */
public class CarMapper {

    public List<Car> getAll() {
        ResultSet res = DB.getDBInstance().executeQuery("select * from car;");
        List<Car> cars = new ArrayList<>();
        try {
            while (res.next()) {
                Car car = new Car();
                car.setCarNr(res.getInt("carid"));
                cars.add(car);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CarMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cars;
    }

    public List<Car> getAvailableCars(Date from, Date to) {
        List<Car> availableCars = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("select * from bookings where (fromDate>='");
        query.append(new java.sql.Date(from.getTime()));
        query.append("' and toDate<='");
        query.append(new java.sql.Date(to.getTime()));
        query.append("') or (fromDate>='");
        query.append(new java.sql.Date(from.getTime()));
        query.append("' and fromDate<='");
        query.append(new java.sql.Date(to.getTime()));
        query.append("') or (toDate>='");
        query.append(new java.sql.Date(from.getTime()));
        query.append("' and toDate<='");
        query.append(new java.sql.Date(to.getTime()));
        query.append("');");
        ResultSet res = DB.getDBInstance().executeQuery(query.toString());
        List<Integer> carsNr = new ArrayList<>();
        try {
            while (res.next()) {
                carsNr.add(res.getInt("carid"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(CarMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<Car> allCars = this.getAll();
        boolean found;
        for (Car car : allCars) {
            found = false;
            for (int k : carsNr) {
                if (k == car.getCarNr()) {
                    found = true;
                }
            }
            if (!found) {
                availableCars.add(car);
            }
        }
        return availableCars;
    }
}
