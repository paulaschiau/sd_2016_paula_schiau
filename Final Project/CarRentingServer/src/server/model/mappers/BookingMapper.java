/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.model.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.model.Booking;
import org.model.Client;
import org.model.Car;

import server.db.DB;

/**
 *
 * @author Paula
 */
public class BookingMapper {

	public void create(Booking booking) {
		StringBuilder query = new StringBuilder();
		query.append("insert into bookings (carid, clientid, fromDate, toDate, pricePerDay) values ('");
		query.append(booking.getCar().getCarNr());
		query.append("','");
		query.append(booking.getClient().getClientID());
		query.append("','");
		query.append(new java.sql.Date(booking.getFromDate().getTime()));
		query.append("','");
		query.append(new java.sql.Date(booking.getToDate().getTime()));
		query.append("','");
		query.append(booking.getPrice());
		query.append("');");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public void update(Booking oldBooking, Booking newBooking) {
		StringBuilder query = new StringBuilder();
		query.append("update bookings set carid='");
		query.append(newBooking.getCar().getCarNr());
		query.append("',clientid='");
		query.append(newBooking.getClient().getClientID());
		query.append("',fromDate='");
		query.append(new java.sql.Date(newBooking.getFromDate().getTime()));
		query.append("',toDate='");
		query.append(new java.sql.Date(newBooking.getToDate().getTime()));
		query.append("' where carid='");
		query.append(oldBooking.getCar().getCarNr());
		query.append("' and clientid='");
		query.append(oldBooking.getClient().getClientID());
		query.append("' and fromDate='");
		query.append(new java.sql.Date(oldBooking.getFromDate().getTime()));
		query.append("' and toDate='");
		query.append(new java.sql.Date(oldBooking.getToDate().getTime()));
		query.append("';");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public List<Booking> readAll() {
		List<Booking> bookings = new ArrayList<>();
		ResultSet res;
		res = DB.getDBInstance().executeQuery("select * from bookings;");
		try {
			while (res.next()) {
				Booking booking = new Booking();
				Client client = new Client();
				Car car = new Car();
				car.setCarNr(res.getInt("carid"));
				client.setClientID(res.getInt("clientid"));
				booking.setClient(client);
				booking.setCar(car);
				booking.setFromDate(res.getDate("fromDate"));
				booking.setToDate(res.getDate("toDate"));
				bookings.add(booking);
			}
		} catch (SQLException e) {
			// TODO
			System.out.println("error in readAll booking mapper");
			e.printStackTrace();
		}
		int i = 0;
		ClientMapper clientMapper = new ClientMapper();
		for (Booking booking : bookings) {
			Client client = clientMapper.read(booking.getClient().getClientID());
			booking.setClient(client);
			bookings.set(i, booking);
			i++;
		}
		return bookings;
	}

	public List<Booking> read(Client client) {
		List<Booking> bookings = new ArrayList<>();
		ResultSet res;
		StringBuilder query = new StringBuilder();
		query.append("select * from bookings where clientid='");
		query.append(client.getClientID());
		query.append("';");
		res = DB.getDBInstance().executeQuery(query.toString());
		try {
			while (res.next()) {
				Booking booking = new Booking();
				Car car = new Car();
				car.setCarNr(res.getInt("carid"));
				booking.setClient(client);
				booking.setCar(car);
				booking.setFromDate(res.getDate("fromDate"));
				booking.setToDate(res.getDate("toDate"));
				bookings.add(booking);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookings;
	}

	public void delete(Booking booking) {
		StringBuilder query = new StringBuilder();
		query.append("delete from bookings where carid='");
		query.append(booking.getCar().getCarNr());
		query.append("' and clientid='");
		query.append(booking.getClient().getClientID());
		query.append("' and fromDate='");
		query.append(new java.sql.Date(booking.getFromDate().getTime()));
		query.append("' and toDate='");
		query.append(new java.sql.Date(booking.getToDate().getTime()));
		query.append("';");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public void deleteBookingsOfClient(Client client) {
		StringBuilder query = new StringBuilder();
		query.append("delete from bookings where clientid='");
		query.append(client.getClientID());
		query.append("';");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public List<Booking> report(Date fromDate, Date toDate) {
		List<Booking> bookings = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		query.append("select * from bookings where fromDate>='");
		query.append(new java.sql.Date(fromDate.getTime()));
		query.append("' and toDate<='");
		query.append(new java.sql.Date(toDate.getTime()));
		query.append("';");
		ResultSet res = DB.getDBInstance().executeQuery(query.toString());
		try {
			while (res.next()) {
				Booking booking = new Booking();
				Client client = new Client();
				Car car = new Car();
				client.setClientID(res.getInt("clientid"));
				car.setCarNr(res.getInt("carid"));
				booking.setClient(client);
				booking.setCar(car);
				booking.setPrice(res.getDouble("pricePerDay"));
				booking.setFromDate(res.getDate("fromDate"));
				booking.setToDate(res.getDate("toDate"));
				bookings.add(booking);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookings;
	}
}
