/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server.model.mappers;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.model.User;

import server.db.DB;

/**
 *
 * @author Paula
 */
public class UserMapper {

	public List<User> getAllUsers() {
		List<User> users = new ArrayList<>();
		ResultSet res = DB.getDBInstance().executeQuery("select * from users;");
		try {
			while (res.next()) {
				User user = new User();
				user.setName(res.getString("name"));
				user.setUsername(res.getString("username"));
				user.setPassword(res.getString("password"));
				user.setType(res.getString("type"));
				users.add(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public void updateUser(User oldUser, User newUser) {
		StringBuilder query = new StringBuilder();
		query.append("update users set name='");
		query.append(newUser.getName());
		query.append("',username='");
		query.append(newUser.getUsername());
		query.append("',password='");
		query.append(newUser.getPassword());
		query.append("' where username='");
		query.append(oldUser.getUsername());
		query.append("';");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public void createUser(User user) {
		StringBuilder query = new StringBuilder();
		query.append("insert into users (name,username,password,type) values ('");
		query.append(user.getName());
		query.append("','");
		query.append(user.getUsername());
		query.append("','");
		query.append(user.getPassword());
		query.append("','");
		query.append(user.getType());
		query.append("');");
		DB.getDBInstance().executeUpdate(query.toString());
	}

	public void deleteUser(User user) {
		StringBuilder query = new StringBuilder();
		query.append("delete from users where username='");
		query.append(user.getUsername());
		query.append("';");
		DB.getDBInstance().executeUpdate(query.toString());
	}
}
