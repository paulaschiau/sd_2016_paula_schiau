/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Paula
 */
public class Booking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1790404315915328027L;
	private Date fromDate;
    private Date toDate;
    private Car car;
    private Client client;
    private double price;

    public Car getCar() {
        return car;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void copy(Booking bookingToCopy) {
        this.client = bookingToCopy.getClient();
        this.fromDate = bookingToCopy.getFromDate();
        this.toDate = bookingToCopy.getToDate();
        this.car = bookingToCopy.getCar();
        this.price = bookingToCopy.getPrice();
    }
}
