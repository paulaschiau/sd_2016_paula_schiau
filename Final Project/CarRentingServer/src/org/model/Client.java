/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paula
 */
public class Client implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8622485751539229052L;
	private int clientID;
    private String firstName;
    private String lastName;
    private String phoneNo;
    private List<Booking> bookings;

    public Client() {
        this.bookings = new ArrayList<>();
    }

    public Client(int clientID, String firstName, String lastName, String phoneNo) {
        this.clientID = clientID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNo = phoneNo;
        this.bookings = new ArrayList<>();
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(List<Booking> reservations) {
        this.bookings = reservations;
    }

}
