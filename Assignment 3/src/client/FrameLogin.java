package client;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;




public class FrameLogin extends JFrame {

	private JFrame frame;
	
	private JComboBox comboBox;
	

	public FrameLogin() {
		frame = new JFrame();
		frame.setTitle("Start");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		String[] doctorList =null;
		
		CSocketClient client = new CSocketClient("localhost", 9992);
		try {
			// connect to server
			client.connect();
			// write message to the server
			SentObject sentObj = new SentObject();
			sentObj.setType("doctor");
			sentObj.setOperation("name list");
			
			client.writeMessage(sentObj);
			// read response from server
			SentObject recievedObj = client.readResponse();
			doctorList = recievedObj.getNameList();
			
		} catch (UnknownHostException e) {
			System.err.println("Host unknown. Cannot establish connection");
		} catch (IOException e) {
			System.err.println("Cannot establish connection. Server may not be up." + e.getMessage());
		}
		
		
		
		JLayeredPane panel = new JLayeredPane();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		ImageIcon img = new ImageIcon("flow_of_glow.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 450, 300);
		label.setIcon(img);
		panel.add(label, -1, 0);

		JButton btnAdmin = new JButton("Admin");
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				FrameAdmin f=new FrameAdmin();

			}
		});
		btnAdmin.setBounds(110, 21, 159, 52);
		panel.add(btnAdmin);

		JButton btnUser = new JButton("Secretary");
		btnUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				FrameSecretary f = new FrameSecretary();

			}
		});
		btnUser.setBounds(110, 84, 159, 52);
		panel.add(btnUser);
		
		comboBox = new JComboBox(doctorList);
		comboBox.setBounds(110, 207, 159, 20);
		panel.add(comboBox);
		
		JButton btnNewButton = new JButton("Doctor");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String selectat = (String) comboBox.getSelectedItem();
				FrameDoctor f = new FrameDoctor(selectat);

			}
		});
		btnNewButton.setBounds(110, 148, 159, 48);
		panel.add(btnNewButton);
		frame.setVisible(true);

	}

	

}

