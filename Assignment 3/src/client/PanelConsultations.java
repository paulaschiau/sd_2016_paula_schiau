package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class PanelConsultations extends JLayeredPane {
	private JTextField textField;
	private JTextField textField_1;
	private JTable table;
	private JPanel panel;

	public PanelConsultations(String nume) {

		ImageIcon img = new ImageIcon("image.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		JLabel lblNewLabel = new JLabel("ID Cons");
		lblNewLabel.setBounds(90, 65, 66, 14);
		this.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Detalii");
		lblNewLabel_1.setBounds(90, 118, 66, 14);
		this.add(lblNewLabel_1);

		textField = new JTextField();
		textField.setBounds(194, 62, 106, 20);
		this.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(194, 115, 106, 20);
		this.add(textField_1);
		textField_1.setColumns(10);

		panel = new JPanel();
		panel.setBounds(344, 69, 425, 215);
		panel.setBackground(Color.WHITE);
		panel.setPreferredSize(new Dimension(500, 500));

		this.add(panel);

		JButton btnNewButton_1 = new JButton("Search");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					int id = Integer.parseInt(textField.getText());

					CSocketClient client = new CSocketClient("localhost", 9992);
					try {
						// conect to server
						client.connect();
						// write message to the server
						SentObject ob_trimis = new SentObject();
						ob_trimis.setType("consultatie");
						ob_trimis.setOperation("cautare");
						ob_trimis.setIdCons(id);

						client.writeMessage(ob_trimis);
						// read response from server
						SentObject ob_primit = client.readResponse();

						textField_1.setText(ob_primit.getConsDetails());

						updateaza(ob_primit.getTable());

					} catch (UnknownHostException e) {
						System.err
								.println("Host unknown. Cannot establish connection");
					} catch (IOException e) {
						System.err
								.println("Cannot establish connection. Server may not be up."
										+ e.getMessage());
					}
					System.out.println("searched consultatie");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton_1.setBounds(256, 338, 89, 23);
		this.add(btnNewButton_1);

		/**
		 * Butonul pentru updatarea unui employee
		 */
		JButton btnNewButton_2 = new JButton("Update");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					int id = Integer.parseInt(textField.getText());
					String detalii = textField_1.getText();

					CSocketClient client = new CSocketClient("localhost", 9990);
					try {
						// conect to server
						client.connect();
						// write message to the server
						SentObject ob_trimis = new SentObject();
						ob_trimis.setType("consultatie");
						ob_trimis.setOperation("update detalii");
						ob_trimis.setIdCons(id);
						ob_trimis.setConsDetails(detalii);

						client.writeMessage(ob_trimis);
						// read response from server
						SentObject ob_primit = client.readResponse();

						updateaza(ob_primit.getTable());

					} catch (UnknownHostException e) {
						System.err
								.println("Host unknown. Cannot establish connection");
					} catch (IOException e) {
						System.err
								.println("Cannot establish connection. Server may not be up."
										+ e.getMessage());
					}
					System.out.println("update consultatie");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton_2.setBounds(430, 338, 89, 23);
		this.add(btnNewButton_2);

	}

	public void updateaza(Object[][] data) {

		String[] columnNames = { "Cons ID", "Pacient ID", "Doctor ID",
				"Detalii" };

		panel.setLayout(null);

		table = new JTable(data, columnNames);
		panel.setLayout(null);
		panel.setLayout(new BorderLayout());
		panel.add(new JScrollPane(table), BorderLayout.CENTER);

		repaint();

	}
}
