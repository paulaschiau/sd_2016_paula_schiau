package client;

import java.io.Serializable;

public class SentObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 12131123234L;
	private String type;
	private String operation;
	private int id;
	private String name;
	private String address;
	private String cnp;
	private int idCons;
	private int idPatient;
	private int idDoc;
	private String consDetails;
	private boolean sosire;
	private String[] nameList;
	private Object[][] table;
	
	public Object[][] getTable() {
		return table;
	}
	public void setTable(Object[][] table) {
		this.table = table;
	}
	public String[] getNameList() {
		return nameList;
	}
	public void setNameList(String[] nameList) {
		this.nameList = nameList;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCnp() {
		return cnp;
	}
	public void setCnp(String cnp) {
		this.cnp = cnp;
	}
	public int getIdCons() {
		return idCons;
	}
	public void setIdCons(int idCons) {
		this.idCons = idCons;
	}
	public int getPatientId() {
		return idPatient;
	}
	public void setPatientId(int idPatient) {
		this.idPatient = idPatient;
	}
	public int getIdDoc() {
		return idDoc;
	}
	public void setIdDoc(int idDoc) {
		this.idDoc = idDoc;
	}
	public String getConsDetails() {
		return consDetails;
	}
	public void setConsDetails(String consDetails) {
		this.consDetails = consDetails;
	}
	public boolean isSosire() {
		return sosire;
	}
	public void setSosire(boolean sosire) {
		this.sosire = sosire;
	}
	
	
	
}
