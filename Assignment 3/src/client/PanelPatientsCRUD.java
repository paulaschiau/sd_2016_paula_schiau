package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.UnknownHostException;


import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class PanelPatientsCRUD extends JLayeredPane {

	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTable table;
	private JPanel panel;

	public PanelPatientsCRUD() {

		ImageIcon img = new ImageIcon("image.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		JLabel lblNewLabel = new JLabel("ID");
		lblNewLabel.setBounds(90, 65, 66, 14);
		this.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Name");
		lblNewLabel_1.setBounds(90, 118, 66, 14);
		this.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Address");
		lblNewLabel_2.setBounds(90, 180, 66, 14);
		this.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("CNP");
		lblNewLabel_3.setBounds(90, 227, 77, 14);
		this.add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBounds(194, 62, 106, 20);
		this.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(194, 115, 106, 20);
		this.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(194, 177, 106, 20);
		this.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(194, 224, 106, 20);
		this.add(textField_3);
		textField_3.setColumns(10);

		panel = new JPanel();
		panel.setBounds(344, 69, 425, 215);
		panel.setBackground(Color.WHITE);
		panel.setPreferredSize(new Dimension(500, 500));

		this.add(panel);

		
		JButton btnNewButton = new JButton("Insert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					int id = Integer.parseInt(textField.getText());
					String nume = textField_1.getText();
					String adresa = textField_2.getText();
					String cnp = textField_3.getText();

					CSocketClient client = new CSocketClient("localhost", 9992);
					try {
						// conect to server
						client.connect();
						// write message to the server
						SentObject ob_trimis = new SentObject();
						ob_trimis.setType("patient");
						ob_trimis.setOperation("insert");
						ob_trimis.setId(id);
						ob_trimis.setName(nume);
						ob_trimis.setAddress(adresa);
						ob_trimis.setCnp(cnp);

						client.writeMessage(ob_trimis);
						// read response from server
						SentObject ob_primit = client.readResponse();
						updateaza(ob_primit.getTable());

					} catch (UnknownHostException e) {
						System.err
								.println("Host unknown. Cannot establish connection");
					} catch (IOException e) {
						System.err
								.println("Cannot establish connection. Server may not be up."
										+ e.getMessage());
					}
					System.out.println("inserted patient");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton.setBounds(90, 338, 89, 23);
		this.add(btnNewButton);

		/**
		 * Butonul pentru cautarea unui employee
		 */
		JButton btnNewButton_1 = new JButton("Search");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					int id = Integer.parseInt(textField.getText());

					CSocketClient client = new CSocketClient("localhost", 9992);
					try {
						// connect to server
						client.connect();
						// write message to the server
						SentObject ob_trimis = new SentObject();
						ob_trimis.setType("patient");
						ob_trimis.setOperation("search");
						ob_trimis.setId(id);

						client.writeMessage(ob_trimis);
						// read response from server
						SentObject ob_primit = client.readResponse();

						
						textField_1.setText(ob_primit.getName());
						textField_2.setText(ob_primit.getAddress());
						textField_3.setText(ob_primit.getCnp());
						
						updateaza(ob_primit.getTable());

					} catch (UnknownHostException e) {
						System.err
								.println("Host unknown. Cannot establish connection");
					} catch (IOException e) {
						System.err
								.println("Cannot establish connection. Server may not be up."
										+ e.getMessage());
					}
					System.out.println("searched pacient");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton_1.setBounds(256, 338, 89, 23);
		this.add(btnNewButton_1);

		
		JButton btnNewButton_2 = new JButton("Update");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					int id = Integer.parseInt(textField.getText());
					String nume = textField_1.getText();
					String adresa = textField_2.getText();
					String cnp = textField_3.getText();

					CSocketClient client = new CSocketClient("localhost", 9992);
					try {
						// conect to server
						client.connect();
						// write message to the server
						SentObject sentObj = new SentObject();
						sentObj.setType("patient");
						sentObj.setOperation("update");
						sentObj.setId(id);
						sentObj.setName(nume);
						sentObj.setAddress(adresa);
						sentObj.setCnp(cnp);

						client.writeMessage(sentObj);
						// read response from server
						SentObject ob_primit = client.readResponse();
						updateaza(ob_primit.getTable());

					} catch (UnknownHostException e) {
						System.err
								.println("Host unknown. Cannot establish connection");
					} catch (IOException e) {
						System.err
								.println("Cannot establish connection. Server may not be up."
										+ e.getMessage());
					}
					System.out.println("update pacient");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton_2.setBounds(430, 338, 89, 23);
		this.add(btnNewButton_2);

		/**
		 * Butonul pentru stergerea unui employee
		 */

		JButton btnNewButton_4 = new JButton("Delete");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {

					int id = Integer.parseInt(textField.getText());

					CSocketClient client = new CSocketClient("localhost", 9992);
					try {
						// conect to server
						client.connect();
						// write message to the server
						SentObject ob_trimis = new SentObject();
						ob_trimis.setType("patient");
						ob_trimis.setOperation("delete");
						ob_trimis.setId(id);

						client.writeMessage(ob_trimis);
						// read response from server
						SentObject ob_primit = client.readResponse();
						
						updateaza(ob_primit.getTable());

					} catch (UnknownHostException e) {
						System.err
								.println("Host unknown. Cannot establish connection");
					} catch (IOException e) {
						System.err
								.println("Cannot establish connection. Server may not be up."
										+ e.getMessage());
					}
					System.out.println("deleted patient");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		;
		btnNewButton_4.setBounds(621, 338, 89, 23);
		this.add(btnNewButton_4);

	}

	public void updateaza(Object[][] data) {

		String[] columnNames = { "Pacient id", "Pacient name", "CNP", "Address" };

		panel.setLayout(null);

		table = new JTable(data, columnNames);
		panel.setLayout(null);
		panel.setLayout(new BorderLayout());
		panel.add(new JScrollPane(table), BorderLayout.CENTER);

		repaint();

	}

}
