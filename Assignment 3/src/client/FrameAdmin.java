package client;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JTabbedPane;


public class FrameAdmin {
private JFrame frame;
	
	
	public FrameAdmin()
	{
	frame = new JFrame();
	frame.setBounds(100, 100, 900, 900);
	frame.setTitle("Clinic");
	frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	frame.setVisible(true);
	

	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	frame.getContentPane().add(tabbedPane);
	tabbedPane.setBounds(0, 0, 800, 800);

	JLayeredPane panel = new PanelDoctorCRUD();
	panel.setBounds(0, 0, 577, 500);	
	panel.setLayout(null);
	tabbedPane.addTab("Doctors", null, panel, null);
	
	
	
	}


	
}