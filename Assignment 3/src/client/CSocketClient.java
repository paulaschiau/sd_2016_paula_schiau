package client;

import java.io.*;
import java.net.*;

public class CSocketClient {

	private String hostname;
	private int port;
	Socket socketClient;

	public CSocketClient(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}

	public void connect() throws UnknownHostException, IOException {
		System.out.println("Attempting to connect to " + hostname + ":" + port);
		socketClient = new Socket(hostname, port);
		System.out.println("Connection Established");
	}

	public SentObject readResponse() throws IOException {

		SentObject object = null;
		ObjectInputStream inputStream = null;

		try {

			System.out.println("Connected to the server.");
			inputStream = new ObjectInputStream(socketClient.getInputStream());
			object = (SentObject) inputStream.readObject();
			inputStream.close();
			System.out.println("Object received by client!");

		} catch (SocketException se) {
			se.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return object;
	}

	
	public void writeMessage(SentObject object) throws IOException {

		ObjectOutputStream outputStream = null;
		try {

			System.out.println("Connected to the server.");
			outputStream = new ObjectOutputStream(socketClient.getOutputStream());// socket->clientsocket

			System.out.println("Object to be written: " + object);
			outputStream.writeObject(object);
			//outputStream.writeObject(new String("I sent one Student object!"));

			//outputStream.close();

			System.out.println("Object sent from client: " + object.toString());
		} catch (SocketException se) {
			se.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String arg[]) {

		FrameLogin f = new FrameLogin();
	}
}
