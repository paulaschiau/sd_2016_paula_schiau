package client;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JTabbedPane;

public class FrameDoctor {
private JFrame frame;
	
	
	public FrameDoctor(String nume)
	{
	frame = new JFrame();
	frame.setBounds(100, 100, 900, 900);
	frame.setTitle("Clinic");
	frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	frame.setVisible(true);
	

	/**
	 * Se construiesc tabbed-panelurile
	 */
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	frame.getContentPane().add(tabbedPane);
	tabbedPane.setBounds(0, 0, 800, 800);

	JLayeredPane panel = new PanelConsultations(nume);
	panel.setBounds(0, 0, 577, 500);	
	panel.setLayout(null);
	tabbedPane.addTab("Consultations", null, panel, null);
	
	
	
	}


	
}