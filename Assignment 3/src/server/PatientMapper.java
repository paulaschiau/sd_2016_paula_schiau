package server;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



public class PatientMapper {
	
	private Connection connection;
	
	public PatientMapper()
	{
		connect();
	}
	
	public void connect(){
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver");
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);
	    	}
	    
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);}
	    	catch(IllegalAccessException ex) {
	    	   System.out.println("Error: access problem while loading!");
	    	   System.exit(2);}
	    	catch(InstantiationException ex) {
	    	   System.out.println("Error: unable to instantiate driver!");
	    	   System.exit(3);
	    	}
	    
			try{
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/clinic?" +
	                    "user=root&password=root");
			} catch (SQLException e) {
		        e.printStackTrace();
		    }

		

	}
	
	
	public  Patient findPatient(int unique_id) {
		try {
		
		String statement = "SELECT patient_id , patient_name, patient_address, patient_cnp FROM patient where patient_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1, unique_id);
		ResultSet rs = dbStatement.executeQuery();
		while(rs.next()) {
		int id = rs.getInt("patient_id");
		String name = rs.getString("patient_name");
		String adress = rs.getString("patient_address");
		String cnp = rs.getString("patient_cnp");
		
		
		Patient e = new Patient();
		e.setName(name);
		e.setAddress(adress);
		e.setCnp(cnp);
		e.setId(id);
		return e;
		}
		return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		}
	
	
	public void updatePatient (Patient x){
		try {
		
		String statement = "UPDATE patient SET patient_id=?, patient_name=?, patient_address=?, patient_cnp=? where patient_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1, x.getId());
		dbStatement.setString(2, x.getName());
		dbStatement.setString(3, x.getAddress());
		dbStatement.setString(4, x.getCnp());
		dbStatement.setInt(5, x.getId());
		dbStatement.executeUpdate();
		}  catch (Exception e) {
			e.printStackTrace();
		}
		}
	
	public void insertPatient(Patient x) {
		try {
		
		String statement = "INSERT INTO patient (patient_id, patient_name, patient_address, patient_cnp) VALUES (?, ?, ?, ?)";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1, x.getId());
		dbStatement.setString(2, x.getName());
		dbStatement.setString(3, x.getAddress());
		dbStatement.setString(4, x.getCnp());
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	
	public void deletePatient (int id){
		try {
		
		String statement = "DELETE FROM patient where patient_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1,id);
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	
	public  ArrayList<Patient> findAllPacients() {
		try {
		
		String statement = "SELECT patient_id, patient_name, patient_address, patient_cnp FROM patient";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		
		ResultSet rs = dbStatement.executeQuery();
		ArrayList<Patient> a = new ArrayList<Patient>();
		
		while(rs.next()) {
			int id = rs.getInt("patient_id");
			String name = rs.getString("patient_name");
			String address = rs.getString("patient_address");
			String cnp = rs.getString("patient_cnp");
		
			
		
			
		a.add(new Patient(id,name,address, cnp));
		
		
		}
		
		return a;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		}
	
}
