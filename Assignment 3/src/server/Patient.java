package server;

public class Patient {
	

		private int id;
		private String name;
		private String address;
		private String cnp;
		
		public Patient(int id, String name, String address, String cnp) {
			super();
			this.id = id;
			this.name = name;
			this.address = address;
			this.cnp = cnp;
		}

		public Patient() {
			// TODO Auto-generated constructor stub
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getCnp() {
			return cnp;
		}

		public void setCnp(String cnp) {
			this.cnp = cnp;
		}

		

}
