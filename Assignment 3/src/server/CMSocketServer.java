package server;

import java.io.*;
import java.net.*;

import client.SentObject;

public class CMSocketServer implements Runnable {
	private Socket connection;
	private int ID;	
	
	public CMSocketServer(Socket connection, int ID) {
		this.connection = connection;
		this.ID = ID;
	}

	public void run() {
		try {
			readFromClient(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void readFromClient(Socket client) throws IOException {

		ObjectInputStream inStream = null;
		try {

			System.out.println("Client connected " + ID);
			inStream = new ObjectInputStream(client.getInputStream());
			ObjectOutputStream outToClient = new ObjectOutputStream(
					client.getOutputStream());
			SentObject receivedObj = (SentObject) inStream.readObject();

			System.out.println("server message received" + receivedObj.getType() + " " + receivedObj.getOperation());

			Model model = new Model();
			SentObject sentObj = model.reply(receivedObj);

			outToClient.writeObject(sentObj);

			inStream.close();
			outToClient.close();
			client.close();

		} catch (SocketException se) {
			se.printStackTrace();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException cn) {
			cn.printStackTrace();
		}
	}

	private void sendMessage(Socket client, String message) throws IOException {
		BufferedWriter writer = new BufferedWriter (new OutputStreamWriter(
				client.getOutputStream()));
		writer.write(message);
		writer.flush();
	}

	public static void main(String[] args) {

		int portNumber = 9992;
		int count = 0;
		System.out.println("Starting the multiple socket server at port: "
				+ portNumber);

		try {
			ServerSocket serverSocket = new ServerSocket(portNumber);
			System.out.println("Multiple Socket Server Initialized");

			while (true) {
				Socket client = serverSocket.accept();
				Runnable runnable = new CMSocketServer(client, ++count);
				Thread thread = new Thread(runnable);
				thread.start();
			}
		} catch (Exception e) {
			System.out.println("Server still running...probably.");
		}
	}
}
