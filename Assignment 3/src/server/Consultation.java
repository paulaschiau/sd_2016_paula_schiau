package server;

public class Consultation {

	private int id;
	private int pacient_id;
	private int doc_id;
	private String detalii;
	
	public Consultation(int id, int pacient_id, int doc_id, String detalii) {
		super();
		this.id = id;
		this.pacient_id = pacient_id;
		this.doc_id = doc_id;
		this.detalii = detalii;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPacient_id() {
		return pacient_id;
	}
	public void setPacient_id(int pacient_id) {
		this.pacient_id = pacient_id;
	}
	public int getDoc_id() {
		return doc_id;
	}
	public void setDoc_id(int doc_id) {
		this.doc_id = doc_id;
	}
	public String getDetails() {
		return detalii;
	}
	public void setDetalii(String detalii) {
		this.detalii = detalii;
	}
	
	
	
	
	
}
