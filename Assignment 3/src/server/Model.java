package server;

import java.util.ArrayList;
import java.util.Observable;


import client.SentObject;

public class Model extends Observable {

	private ArrayList<Consultation> consultationList;
	private ArrayList<Doctor> doctorList;
	private ArrayList<Patient> patientList;
	private int id_pacient_sosit;

	public void patientCheckIn(int id) {
		id_pacient_sosit=id;
		setChanged();
		notifyObservers();
	}
	
	public ArrayList<Consultation> getConsultationList() {
		return consultationList;
	}

	public void setConsultationList(ArrayList<Consultation> consultationList) {
		this.consultationList = consultationList;
	}

	public ArrayList<Doctor> getDoctorList() {
		return doctorList;
	}

	public void setLista_doctori(ArrayList<Doctor> lista_doctori) {
		this.doctorList = lista_doctori;
	}

	public ArrayList<Patient> getLista_pacienti() {
		return patientList;
	}

	public void setLista_pacienti(ArrayList<Patient> lista_pacienti) {
		this.patientList = lista_pacienti;
	}



	public Model() {
		refresh();

	}

	public void refresh() {
		ConsultationMapper cm = new ConsultationMapper();
		DoctorMapper dm = new DoctorMapper();
		PatientMapper pm = new PatientMapper();

		consultationList = cm.findAllConsultations();
		doctorList = dm.findAllDoctors();
		patientList = pm.findAllPacients();
	}

	public String[] getNumeAllDoctors() {
		DoctorMapper em = new DoctorMapper();
		ArrayList<Doctor> al = em.findAllDoctors();
		String[] s = new String[al.size()];
		int k = 0;
		for (Doctor i : al) {
			s[k] = i.getName();
			k++;
		}
		return s;

	}

	public void addPacient(Patient c) {
		PatientMapper a = new PatientMapper();
		a.insertPatient(c);
		refresh();
	}

	public Patient findPacient(int id) {
		PatientMapper a = new PatientMapper();
		return a.findPatient(id);
	}

	public void updatePacient(Patient c) {
		PatientMapper a = new PatientMapper();
		a.updatePatient(c);
		refresh();
	}

	public void deletePacient(int id) {
		PatientMapper a = new PatientMapper();
		a.deletePatient(id);
		refresh();
	}

	public void addDoctor(Doctor c) {
		DoctorMapper m = new DoctorMapper();
		m.insertDoctor(c);
		refresh();
	}

	public Doctor findDoctor(int id) {
		DoctorMapper m = new DoctorMapper();
		return m.findDoctor(id);
	}

	public void updateDoctor(Doctor c) {
		DoctorMapper m = new DoctorMapper();
		m.updateDoctor(c);
		refresh();
	}

	public void deleteDoctor(int id) {
		DoctorMapper m = new DoctorMapper();
		m.deleteDoctor(id);
		refresh();
	}

	

	public void addConsultation(Consultation c) {
		ConsultationMapper m = new ConsultationMapper();
		m.insertConsultation(c);
		refresh();
	}

	public Consultation findConsultation(int id) {
		ConsultationMapper m = new ConsultationMapper();
		return m.findConsultation(id);
	}

	public void updateConsultation(Consultation c) {
		ConsultationMapper m = new ConsultationMapper();
		m.updateConsultation(c);
		refresh();
	}

	public void deleteConsultation(int id) {
		ConsultationMapper m = new ConsultationMapper();
		m.deleteConsultation(id);
		refresh();
	}

	public SentObject reply(SentObject recievedObj) {
		SentObject sentObj = new SentObject();

		if (recievedObj.getType().equals("doctor")) {

			if (recievedObj.getOperation().equals("name list")) {
				sentObj.setNameList(getNumeAllDoctors());
			}
			if (recievedObj.getOperation().equals("insert")) {
				int id = recievedObj.getId();
				String name = recievedObj.getName();
				String address = recievedObj.getAddress();
				Doctor d = new Doctor(id, name, address);
				addDoctor(d);
				sentObj.setTable(generateDoctorTable());
			}

			if (recievedObj.getOperation().equals("update")) {
				int id = recievedObj.getId();
				String name = recievedObj.getName();
				String address = recievedObj.getAddress();
				Doctor d = new Doctor(id, name, address);
				updateDoctor(d);
				sentObj.setTable(generateDoctorTable());
			}

			if (recievedObj.getOperation().equals("search")) {

				int id = recievedObj.getId();

				Doctor d = findDoctor(id);
				sentObj.setTable(generateDoctorTable());
				sentObj.setId(d.getId());
				sentObj.setName(d.getName());
				sentObj.setAddress(d.getAddress());
			
			}
			if (recievedObj.getOperation().equals("delete")) {

				int id = recievedObj.getId();
				deleteDoctor(id);
				sentObj.setTable(generateDoctorTable());
			}

		}

		

		if (recievedObj.getType().equals("patient")) {

			if (recievedObj.getOperation().equals("insert")) {
				int id = recievedObj.getId();
				String nume = recievedObj.getName();
				String address = recievedObj.getAddress();
				String cnp = recievedObj.getCnp();
				Patient d = new Patient(id, nume, cnp, address);
				addPacient(d);
				sentObj.setTable(generatePatientTable());
			}

			if (recievedObj.getOperation().equals("update")) {
				int id = recievedObj.getId();
				String nume = recievedObj.getName();
				String address = recievedObj.getAddress();
				String cnp = recievedObj.getCnp();
				Patient d = new Patient(id, nume, cnp, address);
				updatePacient(d);
				sentObj.setTable(generatePatientTable());
			}

			if (recievedObj.getOperation().equals("search")) {

				int id = recievedObj.getId();

				Patient d = findPacient(id);
				sentObj.setTable(generatePatientTable());
				sentObj.setId(d.getId());
				sentObj.setName(d.getName());
				sentObj.setAddress(d.getAddress());
				sentObj.setCnp(d.getCnp());
			}
			if (recievedObj.getOperation().equals("delete")) {

				int id = recievedObj.getId();
				deletePacient(id);
				sentObj.setTable(generatePatientTable());
			}

		}

		if (recievedObj.getType().equals("consultation")) {

			if (recievedObj.getOperation().equals("insert")) {
				int id = recievedObj.getIdCons();
				int id_pacient = recievedObj.getPatientId();
				int id_doctor = recievedObj.getIdDoc();
				String detalii = recievedObj.getConsDetails();

				Consultation d = new Consultation(id, id_pacient, id_doctor,
						detalii);
				addConsultation(d);
				sentObj.setTable(generateConsulationTable());
			}

			if (recievedObj.getOperation().equals("update")) {
				int id = recievedObj.getIdCons();
				int id_pacient = recievedObj.getPatientId();
				int id_doctor = recievedObj.getIdDoc();
				String detalii = recievedObj.getConsDetails();
				Consultation d = new Consultation(id, id_pacient, id_doctor,
						detalii);
				updateConsultation(d);
				sentObj.setTable(generateConsulationTable());
			}

			if (recievedObj.getOperation().equals("update details")) {

				int id = recievedObj.getIdCons();
				Consultation d = findConsultation(id);
				d.setDetalii(recievedObj.getConsDetails());
				updateConsultation(d);
				sentObj.setTable(generateConsulationTable());
				sentObj.setIdCons(d.getId());
				sentObj.setPatientId(d.getPacient_id());
				sentObj.setIdDoc(d.getDoc_id());
				sentObj.setConsDetails(d.getDetails());

			}

			if (recievedObj.getOperation().equals("search")) {

				int id = recievedObj.getIdCons();

				Consultation d = findConsultation(id);
				sentObj.setTable(generateConsulationTable());
				sentObj.setIdCons(d.getId());
				sentObj.setPatientId(d.getPacient_id());
				sentObj.setIdDoc(d.getDoc_id());
				sentObj.setConsDetails(d.getDetails());
			}

			if (recievedObj.getOperation().equals("delete")) {

				int id = recievedObj.getIdCons();
				deleteConsultation(id);
				sentObj.setTable(generateConsulationTable());
			}

		}
		
		if (recievedObj.getType().equals("arrived client"))
		{
		//patientCheckIn()	
		}

		return sentObj;
	}

	public Object[][] generateDoctorTable() {
		int dim = doctorList.size();
		int k = 0;

		Object[][] data = new Object[dim][3];

		for (Doctor i : doctorList) {

			data[k][0] = i.getId();
			data[k][1] = i.getName();
			data[k][2] = i.getAddress();
	

			k++;

		}

		return data;
	}

	public Object[][] generatePatientTable() {
		int dim = patientList.size();
		int k = 0;

		Object[][] data = new Object[dim][4];

		for (Patient i : patientList) {

			data[k][0] = i.getId();
			data[k][1] = i.getName();
			data[k][2] = i.getAddress();
			data[k][3] = i.getCnp();

			k++;

		}

		return data;
	}



	public Object[][] generateConsulationTable() {
		int dim = consultationList.size();
		int k = 0;

		Object[][] data = new Object[dim][4];

		for (Consultation i : consultationList) {

			data[k][0] = i.getId();
			data[k][1] = i.getPacient_id();
			data[k][2] = i.getDoc_id();
			data[k][3] = i.getDetails();

			k++;

		}

		return data;
	}
}