package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;



public class ConsultationMapper {
	
private Connection connection;
	
	public ConsultationMapper()
	{
		connect();
	}
	
	public void connect() {
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver");
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);
	    	}
	    
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);}
	    	catch(IllegalAccessException ex) {
	    	   System.out.println("Error: access problem while loading!");
	    	   System.exit(2);}
	    	catch(InstantiationException ex) {
	    	   System.out.println("Error: unable to instantiate driver!");
	    	   System.exit(3);
	    	}
	    
			try{
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/clinic?" +
	                    "user=root&password=root");
			} catch (SQLException e) {
		        e.printStackTrace();
		    }

		

	}
	
	

	
	public  Consultation findConsultation(int uniqueID) {
		try {
			
			String statement = "SELECT cons_id, patient_id, doctor_id ,details FROM consultation where cons_id=?";
			PreparedStatement dbStatement = connection.prepareStatement(statement);
			dbStatement.setString(1, Integer.toString(uniqueID));
			ResultSet rs = dbStatement.executeQuery();
			while(rs.next()) {
			int id = rs.getInt("cons_id");
			int pacient_nr = rs.getInt("patient_id");
			int doctor_nr = rs.getInt("doctor_id");			
			String detalii = rs.getString("details");
			
			
			Consultation c = new Consultation(id,pacient_nr,doctor_nr,detalii);
			
			return c;
			}
			return null;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
	}
	
	public void updateConsultation (Consultation x){
		try {
		
		String statement = "UPDATE consultation SET cons_id=?, patient_id=?, doctor_id=?, details=? where cons_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1, x.getId());
		dbStatement.setInt(2, x.getPacient_id());
		dbStatement.setInt(3, x.getDoc_id());
		dbStatement.setString(4, x.getDetails());
		dbStatement.setInt(5, x.getId());
	
		dbStatement.executeUpdate();
		}  catch (Exception e) {
			e.printStackTrace();
		}
		}
	
	public void insertConsultation(Consultation x) {
		try {
		
		String statement = "INSERT INTO consultation (cons_id, patient_id, doctor_id, details) VALUES (?, ?, ?, ?)";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1, x.getId());
		dbStatement.setInt(2, x.getPacient_id());
		dbStatement.setInt(3, x.getDoc_id());
		dbStatement.setString(4, x.getDetails());
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	
	public void deleteConsultation (int id){
		try {
		
		String statement = "DELETE FROM consultation where cons_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1,id);
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	
	public  ArrayList<Consultation> findAllConsultations() {
		try {
		
		String statement = "SELECT cons_id, patient_id, doctor_id, details FROM consultation";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		
		ResultSet rs = dbStatement.executeQuery();
		ArrayList<Consultation> a = new ArrayList<Consultation>();
		
		while(rs.next()) {
			int id = rs.getInt("cons_id");
			int pacient_nr = rs.getInt("patient_id");
			int doctor_nr = rs.getInt("doctor_id");			
			String detalii = rs.getString("details");
			
			/*Doctor e = new Doctor();
			e.setNume(name);
			e.setAdresa(adress);
			e.setId(id);
			e.setCnp(cnp);
		*/
			
		a.add(new Consultation(id,pacient_nr,doctor_nr,detalii));
		
		
		}
		
		return a;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		}
}
