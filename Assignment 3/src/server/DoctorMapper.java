package server;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;






public class DoctorMapper {
	
	private Connection connection;
	
	public DoctorMapper()
	{
		connect();
	}
	
	public void connect() {
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver");
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);
	    	}
	    
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);}
	    	catch(IllegalAccessException ex) {
	    	   System.out.println("Error: access problem while loading!");
	    	   System.exit(2);}
	    	catch(InstantiationException ex) {
	    	   System.out.println("Error: unable to instantiate driver!");
	    	   System.exit(3);
	    	}
	    
			try{
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/clinic?" +
	                    "user=root&password=root");
			} catch (SQLException e) {
		        e.printStackTrace();
		    }

		

	}
	
	
	
	public  Doctor findDoctor(int unique_id) {
		try {
		
		String statement = "SELECT doctor_id , doctor_name, doctor_address FROM doctor where doctor_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1,unique_id);
		ResultSet rs = dbStatement.executeQuery();
		while(rs.next()) {
		int id = rs.getInt("doctor_id");
		String name = rs.getString("doctor_name");
		String address = rs.getString("doctor_address");
	
		
		Doctor e = new Doctor();
		e.setNume(name);
		e.setId(id);
		e.setAddress(address);
		return e;
		}
		return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		}
	
	
	public void updateDoctor (Doctor x){
		try {
		
		String statement = "UPDATE doctor SET doctor_id=?, doctor_name=?, doctor_address=? where doctor_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1, x.getId());
		dbStatement.setString(2, x.getName());
		dbStatement.setString(3, x.getAddress());
		dbStatement.setInt(4, x.getId());
		dbStatement.executeUpdate();
		}  catch (Exception e) {
			e.printStackTrace();
		}
		}
	
	public void insertDoctor(Doctor x) {
		try {
		
		String statement = "INSERT INTO doctor (doctor_id, doctor_name, doctor_address) VALUES (?, ?, ?)";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1, x.getId());
		dbStatement.setString(2, x.getName());
		dbStatement.setString(3, x.getAddress());

		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	
	public void deleteDoctor (int id){
		try {
		
		String statement = "DELETE FROM doctor where doctor_id=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1,id);
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	
	public  ArrayList<Doctor> findAllDoctors() {
		try {
		
		String statement = "SELECT doctor_id, doctor_name, doctor_address FROM doctor";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		
		ResultSet rs = dbStatement.executeQuery();
		ArrayList<Doctor> a = new ArrayList<Doctor>();
		
		while(rs.next()) {
			int id = rs.getInt("doctor_id");
			String name = rs.getString("doctor_name");
			String address = rs.getString("doctor_address");
	
					
		a.add(new Doctor(id,name,address));
		
		
		}
		
		return a;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		}
	
	
	
}

