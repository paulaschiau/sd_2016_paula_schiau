package server;

public class Doctor {
	

		private int id;
		private String nume;
		private String address;
	
		
		public Doctor(int id, String nume, String address) {
			super();
			this.id = id;
			this.nume = nume;
			this.address = address;
			
		}

		public Doctor() {
			// TODO Auto-generated constructor stub
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return nume;
		}

		public void setNume(String nume) {
			this.nume = nume;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}



		

}

