package datasource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;


import domainlogic.Account;
import domainlogic.User;

public class UserMapper {

	private Connection connection;

	public UserMapper()
	{
		connect();
	}
	
	
	public void connect() {
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver");
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);
	    	}
	    
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);}
	    	catch(IllegalAccessException ex) {
	    	   System.out.println("Error: access problem while loading!");
	    	   System.exit(2);}
	    	catch(InstantiationException ex) {
	    	   System.out.println("Error: unable to instantiate driver!");
	    	   System.exit(3);
	    	}
	    
			try{
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank?" +
                    "user=root&password=root");
			} catch (SQLException e) {
		        e.printStackTrace();
		    }

		

	}
	
	
	public  User findUser(String CNP) {
		try {
		
			String statement = "SELECT iduser, CNP, password, name, address FROM user WHERE CNP=? ";
			PreparedStatement dbStatement = connection.prepareStatement(statement);
			dbStatement.setString(1, CNP);
			ResultSet rs = dbStatement.executeQuery();
			while(rs.next()) 
			{
				int id = rs.getInt("iduser");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String address = rs.getString("address");
				
				
				User user = new User();
				user.setUser_id(id);
				user.setName(name);
				user.setPass(password);
				user.setAddress(address);
				user.setCNP(CNP);
				return user;
			}
		return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public User findUserById(int id) {
		try{
			
	
			String statement = "SELECT iduser, CNP, password, name, address FROM user WHERE iduser=? ";
			PreparedStatement dbStatement = connection.prepareStatement(statement);
			dbStatement.setInt(1, id);
			ResultSet rs = dbStatement.executeQuery();
			while(rs.next()) 
			{
				String CNP = rs.getString("CNP");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String address = rs.getString("address");
				
				
				User user = new User();
				user.setUser_id(id);
				user.setName(name);
				user.setPass(password);
				user.setAddress(address);
				user.setCNP(CNP);
				return user;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

		
	}
	public void updateUser (User cl){
		try {
		
		String statement = "UPDATE user SET CNP=?, password=?, name=?, address=? where iduser=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
	
		dbStatement.setString(1, cl.getCNP());
		dbStatement.setString(2, cl.getPassword());
		dbStatement.setString(3, cl.getName());
		dbStatement.setString(4, cl.getAddress());
		dbStatement.setInt(5, cl.getUser_id());
		dbStatement.executeUpdate();
		}  catch (Exception e) {
			e.printStackTrace();
		}
		}
	
	public void insertUser(User cl) {
		try {
		
		String statement = "INSERT INTO user ( CNP, password, name, address) VALUES ( ?, ?, ?, ?)";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		//dbStatement.setInt(1, cl.getUser_id());
		dbStatement.setString(1, cl.getCNP());
		dbStatement.setString(2, cl.getPassword());
		dbStatement.setString(3, cl.getName());
		dbStatement.setString(4, cl.getAddress());
		
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	public void deleteUser (String cnp){
		try {
		
		String statement = "DELETE FROM user where CNP=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setString(1,cnp);
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	public  User[] findAllUsers() {
		try {
		
		String statement = "SELECT iduser, CNP, password, name, address FROM user ";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		
		ResultSet rs = dbStatement.executeQuery();
		ArrayList<User> a = new ArrayList<User>();
		
		while(rs.next()) {
			int id = rs.getInt("iduser");
			String cnp = rs.getString("CNP");
			String password = rs.getString("password");
			String name = rs.getString("name");
			String address = rs.getString("address");
			
			User u = new User();
			u.setCNP(cnp);
			u.setPass(password);
			u.setName(name);
			u.setAddress(address);
			u.setUser_id(id);
		
		a.add(new User(id, cnp, password, name, address));
		
		
		}
		User all[]=a.toArray(new User[a.size()]);
		return all;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public  Account[] findAllAccounts(int userid) {
		try {
		
			String statement = "SELECT idaccount,idaccount_type, iduser, balance, idaccount_type, date_of_creation FROM account where iduser=?";
			PreparedStatement dbStatement = connection.prepareStatement(statement);
			dbStatement.setString(1, Integer.toString(userid));
			ResultSet rs = dbStatement.executeQuery();
			ArrayList<Account> a = new ArrayList<Account>();
			
			while(rs.next()) {
			int acc_id = rs.getInt("idaccount");
			int user_id = rs.getInt("iduser");
			int balance = rs.getInt("balance");
			int type = rs.getInt("idaccount_type");
			//Date date_of_creation = new Date(rs.getDate("date_of_creation").getTime());
			
			a.add(new Account(acc_id, user_id, balance, type, null));
			
			
		}
		Account all[]=a.toArray(new Account[a.size()]);
		return all;
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	
}
