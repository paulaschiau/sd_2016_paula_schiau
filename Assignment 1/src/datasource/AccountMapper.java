package datasource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import domainlogic.Account;


public class AccountMapper {

	private Connection connection;

	public AccountMapper()
	{
		connect();
	}
	
	public void connect() {
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver");
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);
	    	}
	    
	    try {
	    	   Class.forName("com.mysql.jdbc.Driver").newInstance();
	    	}
	    	catch(ClassNotFoundException ex) {
	    	   System.out.println("Error: unable to load driver class!");
	    	   System.exit(1);}
	    	catch(IllegalAccessException ex) {
	    	   System.out.println("Error: access problem while loading!");
	    	   System.exit(2);}
	    	catch(InstantiationException ex) {
	    	   System.out.println("Error: unable to instantiate driver!");
	    	   System.exit(3);
	    	}
	    
			try{
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank?" +
	                    "user=root&password=root");
			} catch (SQLException e) {
		        e.printStackTrace();
		    }

		

	}
	
	public  Account findAccount(int uniqueID) {
		try {
		
		String statement = "SELECT idaccount, idaccount_type, iduser, date_of_creation, balance FROM account where idaccount=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setString(1, Integer.toString(uniqueID));
		ResultSet rs = dbStatement.executeQuery();
		while(rs.next()) {
		int acc_id = rs.getInt("idaccount");
		int type = rs.getInt("idaccount_type");
		int user_id = rs.getInt("iduser");
		int balance = rs.getInt("balance");
		
		Date date_of_creation = new Date(rs.getDate("date_of_creation").getTime());
		
		Account acc = new Account(acc_id, type, user_id, balance, date_of_creation);
		return acc;
		}
		return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		}
	
	public Account findAccountByType(int type,int iduser) {
		try {
			
			String statement = "SELECT idaccount, idaccount_type, iduser, date_of_creation, balance FROM account where idaccount_type=? and iduser=?";
			PreparedStatement dbStatement = connection.prepareStatement(statement);
			dbStatement.setInt(1, type);
			dbStatement.setInt(2, iduser);
			ResultSet rs = dbStatement.executeQuery();
			while(rs.next()) {
			int acc_id = rs.getInt("idaccount");
			//type = rs.getInt("idaccount_type");
			int user_id = rs.getInt("iduser");
			int balance = rs.getInt("balance");
			
			Date date_of_creation = new Date(rs.getDate("date_of_creation").getTime());
			
			Account acc = new Account(acc_id, type, user_id, balance, date_of_creation);
			return acc;
			}
			return null;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
	}
	
	public void updateAccount (Account x){
		try {
		
		String statement = "UPDATE account SET idaccount=?,idaccount_type=?, iduser=?,date_of_creation=?, balance=? where idaccount=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		
		java.sql.Date sqlStartDate1 = new java.sql.Date(x.getDate_of_creation().getTime());
		
		
		dbStatement.setInt(1, x.getAcc_id());
		dbStatement.setInt(2, x.getType());
		dbStatement.setInt(3, x.getUser_id());
		dbStatement.setDate(4, sqlStartDate1);
		dbStatement.setInt(5, x.getBalance());
		dbStatement.setInt(6, x.getAcc_id());
		dbStatement.executeUpdate();
					
		
		
		
		}  catch (Exception e) {
			e.printStackTrace();
		}
		}
	
	public void insertAccount (Account x) {
		try {
		
		String statement = "INSERT INTO account (idaccount, idaccount_type, iduser, date_of_creation, balance) VALUES (?, ?, ?, ?, ?)";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		java.sql.Date sqlStartDate1 = new java.sql.Date(x.getDate_of_creation().getTime());
		dbStatement.setInt(1, x.getAcc_id());
		dbStatement.setInt(2, x.getType());
		dbStatement.setInt(3, x.getUser_id());
		dbStatement.setInt(4, x.getBalance());
		dbStatement.setDate(5, sqlStartDate1);
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}
	
	public void deleteAccount (int id){
		try {
		
		String statement = "DELETE FROM account where idaccount=?";
		PreparedStatement dbStatement = connection.prepareStatement(statement);
		dbStatement.setInt(1,id);
		dbStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		}

	
	
	
	
}
