package domainlogic;

import java.util.ArrayList;


import datasource.AccountMapper;
import datasource.UserMapper;

public class User {
	
	private int user_id;
	private String CNP;
	private String password;
	private String name;

	private String address;
	private ArrayList<Account> acc;
	
	public User(int user_id, String CNP, String password, String name, String address) {
		super();
		this.user_id = user_id;
		this.CNP=CNP;
		this.password = password;
		this.name = name;
		this.address = address;

	
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	
	
	public String getCNP() {
		return CNP;
	}
	
	public void setCNP(String CNP) {
		this.CNP = CNP;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPass(String password) {
		this.password = password;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	
	public ArrayList<Account> getAcc() {
		return acc;
	}

	public void setAcc(ArrayList<Account> acc) {
		this.acc = acc;
	}
	
	public String[] getAllUsers() {
		UserMapper um = new UserMapper();
		User[] es = um.findAllUsers();
		String[] s = new String[es.length];
		for (int i = 0; i < es.length; i++)
			s[i] = es[i].getCNP();

		return s;

	}
	
	public String[] getAllAccounts(int id) {
		UserMapper um = new UserMapper();
		Account[] es = um.findAllAccounts(id);
		String[] s = new String[es.length];
		for (int i = 0; i < es.length; i++)
		{
			
			s[i] = String.valueOf(es[i].getAcc_id());
		}
		return s;

	}

	public boolean checkPass(String cnp, String pass) {
		UserMapper um = new UserMapper();
		User u = um.findUser(cnp);
		
		if (pass.equals(u.getPassword()))
			return true;
		else
			return false;
	}

	public void addUser(User u) {
		UserMapper um = new UserMapper();
		um.insertUser(u);
	}

	public User findUser(String CNP) {
		UserMapper um = new UserMapper();
		return um.findUser(CNP);
	}
	 
	public User findUserById(int id) {
		UserMapper um = new UserMapper();
		return um.findUserById(id);
	}
	
	public void updateUser(User c) {
		UserMapper um = new UserMapper();
		um.updateUser(c);
	}
	
	public void addAccount(Account c) {
		AccountMapper am = new AccountMapper();
		am.insertAccount(c);
	}

	public Account findAccount(int id) {
		AccountMapper am = new AccountMapper();
		return am.findAccount(id);
	}
	
	public Account findAccountByType(int type, int id){
		AccountMapper am = new AccountMapper();
		return am.findAccountByType(type,id);
	}

	public void updateAccount(Account c) {
		AccountMapper am = new AccountMapper();
		am.updateAccount(c);
	}

	public void deleteAccount(int id) {
		AccountMapper am = new AccountMapper();
		am.deleteAccount(id);
	}



}
