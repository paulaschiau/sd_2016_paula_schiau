package domainlogic;


import java.util.Date;

import datasource.AccountMapper;

public class Account {
	private int acc_id;
	private int user_id;
	private int balance;
	private int acc_type;
	private Date date_of_creation;
			
	public Account(int acc_id,int acc_type, int user_id, int balance, Date date_of_creation) {
		super();
		this.acc_id = acc_id;
		this.user_id = user_id;
		this.balance = balance;
		this.acc_type = acc_type;
		this.date_of_creation = date_of_creation;
	}
	
	
	public Account() {
		// TODO Auto-generated constructor stub
	}


	public int getAcc_id() {
		return acc_id;
	}
	
	public void setAcc_id(int acc_id) {
		this.acc_id = acc_id;
	}
	
	public int getType() {
		return acc_type;
	}
	
	public void setType(int acc_type) {
		this.acc_type = acc_type;
	}
	
	public int getBalance() {
		return balance;
	}
	
	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	
	public Date getDate_of_creation() {
		return date_of_creation;
	}
	
	public void setDate_of_creation(Date date_of_creation) {
		this.date_of_creation = date_of_creation;
	}


	public int getUser_id() {
		return user_id;
	}


	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	


	public void transfer (int s, int id_dest)
	{
		int transfered  = 0;
		if(balance >=s) {
			balance=balance-s;
		} else {
			transfered = balance;
			balance = 0;
		}
		AccountMapper am = new AccountMapper();
		am.updateAccount(this);
		Account a = new Account();
		a=am.findAccount(id_dest);
		if(balance >=s) {
			a.setBalance(a.getBalance()+s);
		} else {
			a.setBalance(a.getBalance()+ transfered);
		}
		
		am.updateAccount(a);
		
	}
	
	public void payment (int amount)
	{
		if(amount >= balance){
			balance = balance - amount;
		} else {
			balance = 0;
		}
		AccountMapper am = new AccountMapper();
		am.updateAccount(this);
	}
	

	
}
