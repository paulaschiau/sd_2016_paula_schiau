package presentation;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;


import domainlogic.User;

/**
 * Panoul care contine operatii asupra Clients
 */
public class PanelManageClients extends JLayeredPane {

	private JTextField textFieldCNP;
	private JTextField textFieldPass;
	private JTextField textFieldName;
	private JTextField textFieldDob;
	private JTextField textFieldAddress;
	private JComboBox comboBox;
	
	private User u;

	public PanelManageClients() {

		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		User u = new User();
		//u= u.findUser(user);
		


		JLabel lblNewLabel = new JLabel("CNP");
		lblNewLabel.setBounds(90, 65, 66, 14);
		this.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Password");
		lblNewLabel_1.setBounds(90, 100, 66, 14);
		this.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Name");
		lblNewLabel_2.setBounds(90, 135, 66, 14);
		this.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Date of Birth");
		lblNewLabel_3.setBounds(90, 170, 66, 14);
		this.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Address");
		lblNewLabel_4.setBounds(90, 205, 66, 14);
		this.add(lblNewLabel_4);

		textFieldCNP = new JTextField();
		textFieldCNP.setBounds(194, 62, 106, 20);
		this.add(textFieldCNP);
		textFieldCNP.setColumns(10);
		
		textFieldPass = new JTextField();
		textFieldPass.setBounds(194, 97, 106, 20);
		this.add(textFieldPass);
		textFieldPass.setColumns(10);

		textFieldName = new JTextField();
		textFieldName.setBounds(194, 132, 106, 20);
		this.add(textFieldName);
		textFieldName.setColumns(10);
		
		textFieldDob = new JTextField();
		textFieldDob.setBounds(194, 167, 106, 20);
		this.add(textFieldDob);
		textFieldDob.setColumns(10);
		
		textFieldAddress = new JTextField();
		textFieldAddress.setBounds(194, 202, 106, 20);
		this.add(textFieldAddress);
		textFieldAddress.setColumns(10);

	
		JButton btnNewButton = new JButton("Insert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					int id =4;
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					String CNP = textFieldCNP.getText();
					String nume = textFieldName.getText();
					String adresa = textFieldAddress.getText();
					String pass = textFieldPass.getText();
					User g = new User(id, CNP, pass, nume, adresa);
					g.addUser(g);
					System.out.println("inserted client");
					

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton.setBounds(90, 248, 89, 23);
		this.add(btnNewButton);

	
		JButton btnNewButton_1 = new JButton("Search");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String tt1 = textFieldCNP.getText();

				User c = new User();
				c = c.findUser(tt1);
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				textFieldPass.setText(c.getPassword());
				textFieldName.setText(c.getName());
				textFieldAddress.setText(c.getAddress());

				System.out.println("searched client");
				
			}
		});
		btnNewButton_1.setBounds(200, 248, 89, 23);
		this.add(btnNewButton_1);

		
		JButton btnNewButton_2 = new JButton("Update");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					int user_id =4;
					String CNP = textFieldCNP.getText();
					String nume = textFieldName.getText();
					String address = textFieldAddress.getText();
					String pass = textFieldPass.getText();
					
					
					User g = new User(user_id, CNP, pass, nume, address);
					g.updateUser(g);
					System.out.println("updated client");

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		});
		btnNewButton_2.setBounds(310, 248, 89, 23);
		this.add(btnNewButton_2);

	}

}
