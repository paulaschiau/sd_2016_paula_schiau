package presentation;



import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JTabbedPane;

/**
 * Se construieste panoul de afisare
 */

public class Frame {

	private JFrame frame;
	

	public Frame(String user) {
		frame = new JFrame();
		frame.setBounds(100, 100, 500, 400);
		frame.setTitle("Banca");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setVisible(true);
		

		/**
		 * Se construiesc tabbed-panelurile
		 */
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frame.getContentPane().add(tabbedPane);
		tabbedPane.setBounds(0, 0, 800, 800);

		
		
		
		if (user.equals("admin")) {
			JLayeredPane panel = new PanelManageClients();
			panel.setBounds(0, 0, 577, 500);
			panel.setLayout(null);
			tabbedPane.addTab("Manage Clients", null, panel, null);
			
			JLayeredPane panel_2 = new PanelManageAccounts();
			tabbedPane.addTab("View All Accounts", null, panel_2, null);
			panel_2.setLayout(null);
			
		}
		else
		{
			JLayeredPane panel_1 = new PanelViewAccounts(user);
			tabbedPane.addTab("View Accounts", null, panel_1, null);
			panel_1.setLayout(null);
			
		
			
			JLayeredPane panel_3 = new PanelTransfer(user);
			tabbedPane.addTab("Transfers", null, panel_3, null);
			panel_3.setLayout(null);
			
			JLayeredPane panel_5 = new PanelPayment(user);
			tabbedPane.addTab("Payment", null, panel_5, null);
			panel_5.setLayout(null);
		}
		
	}

	

}

