package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;

import domainlogic.Account;
import domainlogic.User;

/**
 * Panoul care contine operatii asupra Accounts
 */
public class PanelViewAccounts extends JLayeredPane {

	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JComboBox comboBox;
	
	private User u;
	private Account a;

	public PanelViewAccounts(String cnp) {

		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		u = new User();
		u= u.findUser(cnp);
		int userid = u.getUser_id();
		
		String[] acc_list = u.getAllAccounts(userid);
        
		JLabel lblNewLabel_5 = new JLabel("Choose Account ID");
		lblNewLabel_5.setBounds(70, 65, 120, 14);
		this.add(lblNewLabel_5);
		
		//JLabel lblNewLabel = new JLabel("Account ID");
		//lblNewLabel.setBounds(90, 65, 77, 14);
		//this.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Client ID");
		lblNewLabel_1.setBounds(70, 100, 66, 14);
		this.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Balance");
		lblNewLabel_2.setBounds(70, 135, 66, 14);
		this.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Account Type");
		lblNewLabel_3.setBounds(70, 170, 120, 14);
		this.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Date of creation");
		lblNewLabel_4.setBounds(70, 205, 120, 14);
		this.add(lblNewLabel_4);

		//textField = new JTextField();
		//textField.setBounds(194, 62, 106, 20);
		//this.add(textField);
		//textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(194, 97, 106, 20);
		this.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(194, 132, 106, 20);
		this.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(194, 167, 106, 20);
		this.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(194, 202, 106, 20);
		this.add(textField_4);
		textField_4.setColumns(10);
		
		comboBox = new JComboBox(acc_list);
		comboBox.setBounds(194, 62, 106, 20);
		this.add(comboBox);
		
		/**
		 * Butonul pentru cautarea unui account
		 */
		JButton btnNewButton_5 = new JButton("View Details");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//String tt1 = textField.getText();
				String selected = (String) comboBox.getSelectedItem();
				Account c = new Account();
				c = u.findAccount(Integer.parseInt(selected));
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

				textField_1.setText(Integer.toString(c.getUser_id()));
				textField_2.setText(Integer.toString(c.getBalance()));
				if(c.getType()==0)
					textField_3.setText("spending");
				else
					textField_3.setText("savings");
				textField_4.setText(df.format(c.getDate_of_creation()));

				System.out.println("searched account");

			}
		});
		btnNewButton_5.setBounds(306, 62, 105, 23);
		this.add(btnNewButton_5);
		

	}

}
