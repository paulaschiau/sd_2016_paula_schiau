package presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


import domainlogic.User;


public class PanelLogin extends JFrame {

	private JFrame frame;
	private JTextField username;
	private JPasswordField passwordField, passwordField1;
	private JComboBox comboBox;
	private User u;

	public PanelLogin() {
		frame = new JFrame();
		frame.setTitle("Start");
		frame.setBounds(100, 100, 450, 330);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLayeredPane panel = new JLayeredPane();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 450, 300);
		label.setIcon(img);
		panel.add(label, -1, 0);

		JButton btnAdmin = new JButton("Admin Login");
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				char[] input = passwordField1.getPassword();
				if (isAdminPasswordCorrect(input)) {

					Frame f = new Frame("admin");
				} else {
					JOptionPane.showMessageDialog(null, "Login unsuccessful");
				}

			}
		});
		btnAdmin.setBounds(110, 205, 159, 52);
		panel.add(btnAdmin);

		u = new User();
		String[] cnp_list = u.getAllUsers();
		
		JButton btnUser = new JButton("User Login");
		btnUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String selected = (String) comboBox.getSelectedItem();
				String pass = new String (passwordField.getPassword());
				
				
				if (u.checkPass(selected, pass)) {

					Frame f = new Frame(selected);
				} else {
					JOptionPane.showMessageDialog(null, "Login unsuccessful");
				}
				

			}
		});
		btnUser.setBounds(110, 104, 159, 52);
		panel.add(btnUser);
		repaint();

		passwordField = new JPasswordField();
		passwordField.setBounds(184, 65, 140, 20);
		panel.add(passwordField);

		JLabel lblCNP = new JLabel("CNP:");
		lblCNP.setForeground(Color.white);
		lblCNP.setBounds(80, 32, 80, 14);
		panel.add(lblCNP);
		
		JLabel lblPass = new JLabel("Password:");
		lblPass.setForeground(Color.white);
		lblPass.setBounds(80, 67, 80, 14);
		panel.add(lblPass);
		
		JLabel lblAdminPass = new JLabel("Admin Password:");
		lblAdminPass.setForeground(Color.white);
		lblAdminPass.setBounds(80, 173, 100, 14);
		panel.add(lblAdminPass);
		
		comboBox = new JComboBox(cnp_list);
		comboBox.setBounds(184, 30, 140, 20);
		panel.add(comboBox);
		 
		passwordField1 = new JPasswordField();
		passwordField1.setBounds(184, 170, 140, 20);
		panel.add(passwordField1);
		
		frame.setVisible(true);

	}

	/**
	 * @param input
	 * @return true daca parola este corecta si false in caz contrar
	 */
	private static boolean isAdminPasswordCorrect(char[] input) {
		boolean isCorrect = true;
		char[] correctPassword = { 'a', 'd', 'm', 'i', 'n' };

		if (input.length != correctPassword.length) {
			isCorrect = false;
		} else {
			isCorrect = Arrays.equals(input, correctPassword);
		}

		// Zero out the password.
		Arrays.fill(correctPassword, '0');

		return isCorrect;
	}

}
