package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;

import domainlogic.Account;
import domainlogic.User;


public class PanelTransfer extends JLayeredPane {

	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JComboBox comboBox, comboBox1;
	private User u;

	public PanelTransfer(String cnp) {

		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		u = new User();
		u= u.findUser(cnp);
		int userid = u.getUser_id();
		String[] acc_list = u.getAllAccounts(userid);

		JLabel lblNewLabel = new JLabel("Transfer from:");
		lblNewLabel.setBounds(70, 65, 120, 14);
		this.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("To:");
		lblNewLabel_2.setBounds(70, 100, 120, 14);
		this.add(lblNewLabel_2);
		
		JLabel lblNewLabel_1 = new JLabel("Amount:");
		lblNewLabel_1.setBounds(70, 135, 120, 14);
		this.add(lblNewLabel_1);

		

		comboBox = new JComboBox(acc_list);
		comboBox.setBounds(194, 62, 106, 20);
		this.add(comboBox);
		
		comboBox1 = new JComboBox(acc_list);
		comboBox1.setBounds(194, 97, 106, 20);
		this.add(comboBox1);
		

	

		textField_1 = new JTextField();
		textField_1.setBounds(194, 132, 106, 20);
		this.add(textField_1);
		textField_1.setColumns(10);

	
		
		JButton btnNewButton = new JButton("Transfer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					String selected =(String) comboBox.getSelectedItem();
					String selected1 =(String) comboBox1.getSelectedItem();
					Account c = new Account();
					c = u.findAccount(Integer.parseInt(selected));
					c.getBalance();
					c.transfer(Integer.parseInt(textField_1.getText()), Integer.parseInt(selected1));
					System.out.println("transfer");
					

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton.setBounds(90, 190, 89, 23);
		this.add(btnNewButton);

	

		

	}

	
	}
