package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;

import domainlogic.Account;
import domainlogic.User;

/**
 * Panoul care contine operatii asupra Accounts
 */
public class PanelManageAccounts extends JLayeredPane {

	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JComboBox comboBox, comboBox1;
	
	private User u;
	private Account c;

	public PanelManageAccounts() {

		ImageIcon img = new ImageIcon("image2.jpg");
		JLabel label = new JLabel();
		label.setBounds(0, 0, 900, 900);
		label.setIcon(img);
		this.add(label, -1, 0);

		u = new User();
		
		String[] cnp_list = u.getAllUsers();
		String[] acc_list = {"savings", "spending"};
        
		JLabel lblNewLabel_5 = new JLabel("Choose Client");
		lblNewLabel_5.setBounds(70, 65, 120, 14);
		this.add(lblNewLabel_5);
		
		JLabel lblNewLabel = new JLabel("Account Type");
		lblNewLabel.setBounds(70, 100, 77, 14);
		this.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Client ID");
		lblNewLabel_1.setBounds(70, 135, 66, 14);
		this.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Balance");
		lblNewLabel_2.setBounds(70, 170, 66, 14);
		this.add(lblNewLabel_2);


		JLabel lblNewLabel_4 = new JLabel("Date of creation");
		lblNewLabel_4.setBounds(70, 200, 120, 14);
		this.add(lblNewLabel_4);

		//textField = new JTextField();
		//textField.setBounds(194, 62, 106, 20);
		//this.add(textField);
		//textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(194, 132, 106, 20);
		this.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(194, 167, 106, 20);
		this.add(textField_2);
		textField_2.setColumns(10);



		textField_4 = new JTextField();
		textField_4.setBounds(194, 202, 106, 20);
		this.add(textField_4);
		textField_4.setColumns(10);
		
		
		
		comboBox1 = new JComboBox(cnp_list);
		comboBox1.setBounds(194, 62, 106, 20);
		this.add(comboBox1);
		
		comboBox = new JComboBox(acc_list);
		comboBox.setBounds(194, 97, 106, 20);
		this.add(comboBox);
		
		/**
		 * Butonul pentru cautarea unui account
		 */
		JButton btnNewButton_5 = new JButton("View Account");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				//String tt1 = textField.getText();
				String selected = (String) comboBox1.getSelectedItem();
				String selected1 = (String) comboBox.getSelectedItem();
				u= u.findUser(selected);
				int userid = u.getUser_id();
			
				
				c = new Account();
				if(selected1.equals("spending")){
									
					c = u.findAccountByType(0,userid);
					System.out.println("AWSEDRFTGYU");
				}
				else if(selected1.equals("savings")){
					c = u.findAccountByType(1,userid);
				}
				DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

				textField_1.setText(Integer.toString(c.getUser_id()));
				textField_2.setText(Integer.toString(c.getBalance()));
				//if(c.getType()==0)
					//textField_3.setText("spending");
				//else
					//textField_3.setText("savings");
				//textField_3.setText(selected1);
				textField_4.setText(df.format(c.getDate_of_creation()));

				System.out.println("searched account");

			}
		});
		btnNewButton_5.setBounds(306, 96, 125, 23);
		this.add(btnNewButton_5);

		
		
		/*JButton btnNewButton = new JButton("Insert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					int acc_id = Integer.parseInt(textField.getText());
					int user_id = Integer.parseInt(textField_1.getText());
					int balance = Integer.parseInt(textField_2.getText());
					int type = Integer.parseInt(textField_3.getText());
					Date date1 = (Date) new SimpleDateFormat("dd/MM/yyyy").parse(textField_4.getText());
					Account g = new Account(acc_id, user_id, balance, type, date1);
					u.addAccount(g);
					System.out.println("inserted account");
					//u.log_operatii(u.getName(), "insert account");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		btnNewButton.setBounds(90, 338, 89, 23);
		this.add(btnNewButton);



		JButton btnNewButton_2 = new JButton("Update");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {

					int acc_id = Integer.parseInt(textField.getText());
					int user_id = Integer.parseInt(textField_1.getText());
					int balance= Integer.parseInt(textField_2.getText());
					int type = Integer.parseInt(textField_3.getText());
					Date date1 = (Date) new SimpleDateFormat("dd/MM/yyyy").parse(textField_4.getText());
					Account g = new Account(acc_id, user_id, balance, type, date1);

					u.updateAccount(g);
					System.out.println("updated account");
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		});
		btnNewButton_2.setBounds(430, 338, 89, 23);
		this.add(btnNewButton_2);


		JButton btnNewButton_4 = new JButton("Delete");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					u.deleteAccount(Integer.parseInt(textField.getText()));
					System.out.println("deleted account");
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnNewButton_4.setBounds(621, 338, 89, 23);
		this.add(btnNewButton_4);

	}*/
	}

}
