SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `bank` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bank` ;

-- -----------------------------------------------------
-- Table `bank`.`user_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`user_type` (
  `iduser_type` INT NOT NULL,
  `user_type_name` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`iduser_type`),
  UNIQUE INDEX `iduser_type_UNIQUE` (`iduser_type` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bank`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`user` (
  `iduser` INT NOT NULL,
  `iduser_type` INT NOT NULL,
  `tel` VARCHAR(45) NOT NULL,
  `username` VARCHAR(15) NOT NULL,
  `password` VARCHAR(15) NOT NULL,
  `name` VARCHAR(15) NOT NULL,
  `surname` VARCHAR(25) NOT NULL,
  `dob` DATE NOT NULL,
  `country` VARCHAR(25) NOT NULL,
  `city` VARCHAR(25) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `CNP` VARCHAR(13) NOT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE INDEX `idclient_UNIQUE` (`iduser` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `iduser_type_idx` (`iduser_type` ASC),
  CONSTRAINT `iduser_type`
    FOREIGN KEY (`iduser_type`)
    REFERENCES `bank`.`user_type` (`iduser_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bank`.`currency`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`currency` (
  `idcurrency` INT NOT NULL,
  `currency_name` VARCHAR(4) NOT NULL,
  UNIQUE INDEX `idcurrency_UNIQUE` (`idcurrency` ASC),
  PRIMARY KEY (`idcurrency`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bank`.`account_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`account_type` (
  `idaccount_type` INT NOT NULL,
  `account_type_name` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idaccount_type`),
  UNIQUE INDEX `idaccount_type_UNIQUE` (`idaccount_type` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bank`.`account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bank`.`account` (
  `idaccount` INT NOT NULL AUTO_INCREMENT,
  `idaccount_type` INT NOT NULL,
  `iduser` INT NOT NULL,
  `date_of_creation` DATE NOT NULL,
  `balance` INT NOT NULL,
  `idcurrency` INT NOT NULL,
  UNIQUE INDEX `idaccount_UNIQUE` (`idaccount` ASC),
  PRIMARY KEY (`idaccount`),
  INDEX `idclient_idx` (`iduser` ASC),
  INDEX `idcurrency_idx` (`idcurrency` ASC),
  INDEX `idaccount_type_idx` (`idaccount_type` ASC),
  CONSTRAINT `iduser`
    FOREIGN KEY (`iduser`)
    REFERENCES `bank`.`user` (`iduser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idcurrency`
    FOREIGN KEY (`idcurrency`)
    REFERENCES `bank`.`currency` (`idcurrency`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idaccount_type`
    FOREIGN KEY (`idaccount_type`)
    REFERENCES `bank`.`account_type` (`idaccount_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
